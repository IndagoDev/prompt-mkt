import './index.scss';

import { Link, useNavigate } from 'react-router-dom';
import { MouseEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { BootstrapIcon } from '../bootstrapIcon';
import { MarketplaceService } from '../../api/services/marketplaceService';
import { RootState } from '../../store/store';
import { setPromptBuyDialog } from '../../store/slices/appSlice';
import { useAsyncProcessManagerContext } from '../../tools/asyncProcessManager/context';

export interface ProductCardProps {
  id: number;
  creator: number;
  category: string;
  name: string;
  price: string;
  image: string;
  is_liked: boolean;
  paid: boolean;
}

export function ProductCard(props: ProductCardProps) {
  const asyncProcessManager = useAsyncProcessManagerContext();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const isAuthorized = useSelector(
    (state: RootState) => state.user.isAuthorized
  );
  const currentUser = useSelector((state: RootState) => state.user.currentUser);

  const [isLiked, setIsLiked] = useState<boolean>(props.is_liked);

  useEffect(() => {
    setIsLiked(props.is_liked);
  }, [props.is_liked]);

  function handleOnLike(e: MouseEvent<HTMLButtonElement>) {
    e.preventDefault();

    asyncProcessManager?.doProcess({
      name: 'Toggle like',
      action: async () => {
        if (isLiked) {
          await MarketplaceService.deleteLikePrompt({
            promptId: props?.id ?? 0,
          });
        } else {
          await MarketplaceService.likePrompt({
            sender: currentUser?.id ?? 0,
            receiver: props?.id ?? 0,
          });
        }

        setIsLiked(!isLiked);
      },
    });
  }

  function handleOnBuy(e: MouseEvent<HTMLButtonElement>) {
    e.preventDefault();

    dispatch(
      setPromptBuyDialog({
        active: true,
        id: props.id,
        creator: props.creator,
        amount: +props.price ?? 0,
        currency: '$',
      })
    );
  }

  let image = props.image;
  return (
    <Link to={'/marketplace/' + props.id}>
      <article
        className='product-card fade-in'
        style={{ backgroundImage: `url(${image})` }}
      >
        <div className='category'>{props.category}</div>
        <div className='title'>
          <div className='name'>{props.name}</div>
          <div className='actions'>
            {isAuthorized && (
              <button className='like-button' onClick={handleOnLike}>
                <BootstrapIcon icon={isLiked ? 'heart-fill' : 'heart'} />
              </button>
            )}
            {!props.paid && (
              <button className='price' onClick={handleOnBuy}>
                <BootstrapIcon icon='bag' />
                <span>{props.price}$</span>
              </button>
            )}
          </div>
        </div>
      </article>
    </Link>
  );
}
