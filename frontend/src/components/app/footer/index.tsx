import './index.scss';

import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export function Footer() {
  const { t } = useTranslation();

  return (
    <footer className='app-footer'>
      <p>
        {t('footer.copyright', {
          year: new Date().getFullYear(),
        })}
      </p>

      <div className='links'>
        <Link to={'/faq'}>{t('footer.faq')}</Link>
        <Link to={'/contact'}>{t('footer.contact')}</Link>
        <Link to={'/privacy'}>{t('footer.privacyPolicy')}</Link>
        <Link to={'/cookie'}>{t('footer.cookiePolicy')}</Link>
        <Link to={'/terms'}>{t('footer.termsOfUse')}</Link>
      </div>
    </footer>
  );
}
