import './index.scss';

import { useDispatch, useSelector } from 'react-redux';

import { Button } from '../../form/button';
import { Dialog } from '../../dialog';
import { MarketplaceService } from '../../../api/services/marketplaceService';
import { RootState } from '../../../store/store';
import { setPromptBuyDialog } from '../../../store/slices/appSlice';
import { useAsyncProcessManagerContext } from '../../../tools/asyncProcessManager/context';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export interface IPromptBuyDialog {
  active: boolean;
  id: number;
  amount: number;
  currency: string;
  creator: number;
}

export function PromptBuyDialog(props: IPromptBuyDialog) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const asyncProcessManager = useAsyncProcessManagerContext();

  const promptBuyDialog = useSelector(
    (state: RootState) => state.app.promptBuyDialog
  );
  const currentUser = useSelector((state: RootState) => state.user.currentUser);

  function onNo() {
    dispatch(
      setPromptBuyDialog({
        ...promptBuyDialog,
        active: false,
      })
    );
  }

  function onYes() {
    asyncProcessManager?.doProcess({
      name: t('promptBuy.confirmBuyPrompt'),
      action: async () => {
        await MarketplaceService.createOrder({
          prompt: promptBuyDialog.id,
          buyer: currentUser?.id ?? 0,
          creator: promptBuyDialog.creator,
          price: promptBuyDialog.amount.toString(),
        });

        dispatch(
          setPromptBuyDialog({
            ...promptBuyDialog,
            active: false,
          })
        );

        navigate('/profile/orders');
      },
    });
  }

  return (
    <Dialog className='prompt-buy-dialog' active={props.active}>
      <h2>{t('promptBuy.purchaseConfirmation')}</h2>
      <p>
        <span>
          {t('promptBuy.confirmBuyPrompt')}{' '}
        </span>
        <b>
          {props.amount}
          {props.currency}
        </b>
        ?
      </p>
      <div className='buttons'>
        <Button text={t('promptBuy.no')} onClick={onNo} />
        <Button primary={true} text={t('promptBuy.yes')} onClick={onYes} />
      </div>
    </Dialog>
  );
}
