import './index.scss';

import { NavLink, useNavigate, useSearchParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

import BurgerMenu from './components/burgerMenu';
import { ClientTools } from '../../../tools/client';
import { HeaderItem } from './components/headerItem';
import LanguageChange from '../languageChange';
import Logo from '../../../assets/svg/logo.svg';
import { RootState } from '../../../store/store';
import { SearchBar } from './components/searchBar';
import { setSearchQuery } from '../../../store/slices/appSlice';
import { useTranslation } from 'react-i18next';

export function Header() {
  const dispatch = useDispatch();
  const searchQuery = useSelector((state: RootState) => state.app.searchQuery);

  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const isAuthorized = useSelector(
    (state: RootState) => state.user.isAuthorized
  );

  const { t, i18n } = useTranslation();

  const currentLanguage = i18n.language;

  const [instanstSearchQuery, setInstantSearchQuery] = useState<string>(
    new URLSearchParams(window.location.search).get('q') ?? ''
  );

  useEffect(() => {
    if (instanstSearchQuery !== '') searchParams.set('q', instanstSearchQuery);
    else searchParams.delete('q');

    let timeout = setTimeout(() => {
      dispatch(setSearchQuery(instanstSearchQuery));
      navigate(`?${searchParams.toString()}`, { replace: true });
    }, 300);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [instanstSearchQuery]);

  useEffect(() => {
    if (searchQuery === '') {
      dispatch(
        setSearchQuery(
          new URLSearchParams(window.location.search).get('q') ?? ''
        )
      );
    }
  }, []);

  return (
    <header className='app-header'>
      <NavLink to={'/'}>
        <img className='logo' alt='prompt-mkt' src={Logo} />
      </NavLink>
      <BurgerMenu />

      <SearchBar
        value={instanstSearchQuery}
        onSearch={(query: string) => setInstantSearchQuery(query)}
      />
      <HeaderItem title={t('common.marketplace')} link='marketplace' />
      <HeaderItem title={t('common.sell')} link='sell' />
      {isAuthorized ? (
        <>
          <HeaderItem title={t('common.profile')} link='profile' />
          <HeaderItem title={t('common.logout')} link='auth/logout' />
        </>
      ) : (
        <>
          <HeaderItem title={t('common.login')} link='auth/login' />
          <HeaderItem title={t('common.signup')} link='auth/signup' />
        </>
      )}

      <LanguageChange
        onChange={(lang: string) => {
          localStorage.setItem('lang', lang);
          i18n.changeLanguage(lang);
        }}
        language={currentLanguage}
      />
    </header>
  );
}
