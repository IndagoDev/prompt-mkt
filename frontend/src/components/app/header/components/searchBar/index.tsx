import './index.scss';

import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

import { BootstrapIcon } from '../../../../bootstrapIcon';
import { RootState } from '../../../../../store/store';
import { setSearchQuery } from '../../../../../store/slices/appSlice';
import { useTranslation } from 'react-i18next';

export interface ISearchBar {
  value: string;
  onSearch: (query: string) => void;
  onButtonPress?: () => void;
}

export function SearchBar(props: ISearchBar) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const searchQuery = useSelector((state: RootState) => state.app.searchQuery);

  const { t } = useTranslation();

  const [searchParams] = useSearchParams();

  const [instanstSearchQuery, setInstantSearchQuery] = useState<string>(
    new URLSearchParams(window.location.search).get('q') ?? ''
  );

  useEffect(() => {
    if (instanstSearchQuery !== '') searchParams.set('q', instanstSearchQuery);
    else searchParams.delete('q');

    let timeout = setTimeout(() => {
      dispatch(setSearchQuery(instanstSearchQuery));
      navigate(`?${searchParams.toString()}`, { replace: true });
    }, 300);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [instanstSearchQuery]);

  useEffect(() => {
    if (searchQuery === '') {
      dispatch(
        setSearchQuery(
          new URLSearchParams(window.location.search).get('q') ?? ''
        )
      );
    }
  }, []);

  return (
    <div className='search-bar'>
      <input
        onChange={(e) => props.onSearch(e.target.value)}
        placeholder={t('common.search')}
        type='search'
        name='q'
        value={props.value}
        onKeyUp={(e) => {
          if (e.key === 'Enter') {
            navigate({
              pathname: '/marketplace',
              search: window.location.search,
            });
          }
        }}
      />
      <Link onClick={() => props.onButtonPress && props.onButtonPress()} to={{
        pathname: '/marketplace',
        search: window.location.search
      }}>
        <BootstrapIcon icon='search' />
      </Link>
    </div>
  );
}
