import './index.scss';

import { NavLink, useNavigate, useSearchParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

import { AnimationTools } from '../../../../../tools/animation';
import { Button } from '../../../../form/button';
import { RootState } from '../../../../../store/store';
import { SearchBar } from '../searchBar';
import { setSearchQuery } from '../../../../../store/slices/appSlice';
import { useTranslation } from 'react-i18next';

export default function BurgerMenu() {
  const [isActive, setIsActive] = useState<boolean>(false);

  const [isDetached, setIsDetached] = useState<boolean>(false);
  const [isHidden, setIsHidden] = useState<boolean>(false);

  const { t } = useTranslation();

  useEffect(() => {
    AnimationTools.autoShowHideTransition(isActive, setIsDetached, setIsHidden);
  }, [isActive]);

  const dispatch = useDispatch();
  const searchQuery = useSelector((state: RootState) => state.app.searchQuery);

  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const isAuthorized = useSelector(
    (state: RootState) => state.user.isAuthorized
  );

  const [instanstSearchQuery, setInstantSearchQuery] = useState<string>(
    new URLSearchParams(window.location.search).get('q') ?? ''
  );

  useEffect(() => {
    if (instanstSearchQuery !== '') searchParams.set('q', instanstSearchQuery);
    else searchParams.delete('q');

    let timeout = setTimeout(() => {
      dispatch(setSearchQuery(instanstSearchQuery));
      navigate(`?${searchParams.toString()}`, { replace: true });
    }, 300);

    return () => clearTimeout(timeout);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [instanstSearchQuery]);

  useEffect(() => {
    if (searchQuery === '') {
      dispatch(
        setSearchQuery(
          new URLSearchParams(window.location.search).get('q') ?? ''
        )
      );
    }
  }, []);

  return (
    <div>
      <Button
        onClick={() => {
          setIsActive(!isActive);
        }}
        size='mini'
        icon='list'
        className='burger-menu-toggle'
      />
      {!isDetached && (
        <div className={'burger-menu' + (isHidden ? ' hidden' : '')}>
          <SearchBar
            value={instanstSearchQuery}
            onSearch={(query: string) => setInstantSearchQuery(query)}
            onButtonPress={() => setIsActive(false)}
          />
          <Button
            onClick={() => setIsActive(false)}
            text={t('common.marketplace')}
            isLink={true}
            linkTo='marketplace'
          />
          <Button
            onClick={() => setIsActive(false)}
            text={t('common.sell')}
            isLink={true}
            linkTo='sell'
          />
          {isAuthorized ? (
            <>
              <Button
                onClick={() => setIsActive(false)}
                text={t('common.profile')}
                isLink={true}
                linkTo='profile'
              />
              <Button
                onClick={() => setIsActive(false)}
                text={t('common.logout')}
                isLink={true}
                linkTo='auth/logout'
              />
            </>
          ) : (
            <>
              <Button
                onClick={() => setIsActive(false)}
                text={t('common.login')}
                isLink={true}
                linkTo='auth/login'
              />
              <Button
                onClick={() => setIsActive(false)}
                text={t('common.signup')}
                isLink={true}
                linkTo='auth/signup'
              />
            </>
          )}
        </div>
      )}
    </div>
  );
}
