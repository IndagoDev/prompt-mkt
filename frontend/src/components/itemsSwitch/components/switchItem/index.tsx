import { MouseEvent, MouseEventHandler, createRef } from 'react';

import { BootstrapIcon } from '../../../bootstrapIcon';

export interface ISwitchItem {
  value: number | string;
  title?: string;
  icon?: string;
  error?: boolean;
  screenReaderInfo?: string;
}

export interface SwitchItemProps {
  id: string;
  checked?: boolean;
  isTabList?: boolean;
  roveIndex?: number;
  onClick?: MouseEventHandler<HTMLButtonElement>;
}

export function SwitchItem(props: SwitchItemProps & ISwitchItem) {
  const ref = createRef<HTMLButtonElement>();

  function handleOnClick(e: MouseEvent<HTMLButtonElement>) {
    if (props.onClick) props.onClick(e);
  }

  return (
    <button
      type='button'
      className={
        'switch-item' +
        (props.checked ? ' checked' : '') +
        (props.error ? ' error' : '')
      }
      role={props.isTabList ? 'tab' : undefined}
      aria-selected={props.isTabList ? props.checked : undefined}
      onClick={handleOnClick}
      ref={ref}
    >
      {props.icon && (
        <BootstrapIcon
          icon={props.error ? 'exclamation-circle-fill' : props.icon}
        />
      )}
      {props.title && <span className='title'>{props.title}</span>}
    </button>
  );
}
