import './index.scss';

import { ISwitchItem, SwitchItem } from './components/switchItem';
import { useEffect, useId, useState } from 'react';

export interface IItemsSwitch {
  items: ISwitchItem[];
  isTabList?: boolean;
  vertical?: boolean;
  value?: number | string;
  onChange?: (value?: string | number) => void;
}

export function ItemsSwitch(props: IItemsSwitch) {
  const id = useId();

  const [value, setValue] = useState<number | string | undefined>(
    props.value ?? undefined
  );

  useEffect(() => {
    if (value !== props.value) {
      if (props.onChange) props.onChange(value);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  useEffect(() => {
    if (props.value !== value) {
      setValue(props.value ?? undefined);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.value]);

  return (
    <div
      className={'items-switch' + (props.vertical ? ' vertical' : '')}
      role={props.isTabList ? 'tablist' : undefined}
    >
      {props.items.map((item, index) => {
        return (
          <SwitchItem
            onClick={() => {
              setValue(item.value);
            }}
            roveIndex={index}
            key={index}
            id={id}
            isTabList={props.isTabList}
            checked={value === item.value}
            {...item}
          />
        );
      })}
    </div>
  );
}
