import './index.css';
import './theme.css';

import i18next, { InitOptions } from 'i18next';

import App from './App';
import { ClientTools } from './tools/client';
import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Toaster } from 'react-hot-toast';
import reportWebVitals from './reportWebVitals';
import store from './store/store';
import translationEN from './assets/i18n/en.json';
import translationUA from './assets/i18n/ua.json';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

i18next.init({
  lng: ClientTools.getPrefferedLanguage(),
  interpolation: {
    prefix: '{',
    suffix: '}',
  },
  resources: {
    en: {
      translation: translationEN,
    },
    ua: {
      translation: translationUA,
    }
  },
  //debug: true,
} as InitOptions);


root.render(
  <Provider store={store}>
    <I18nextProvider i18n={i18next}>
      <Toaster
        toastOptions={{
          style: {
            background: 'var(--ui-background-body)',
            color: 'var(--ui-text)',
          },
        }}
      />
      <App />
    </I18nextProvider>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
