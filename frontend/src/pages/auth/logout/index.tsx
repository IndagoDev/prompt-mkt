import './index.scss';

import {
  setCurrentUser,
  setIsAuthorized,
} from '../../../store/slices/userSlice';
import { shopApi, userApi } from '../../../api';

import { AuthService } from '../../../api/services/authService';
import { useAsyncProcessManagerContext } from '../../../tools/asyncProcessManager/context';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

export function LogOutPage() {
  const dispatch = useDispatch();
  const asyncProcessManager = useAsyncProcessManagerContext();
  const { t } = useTranslation();

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Do logout',
      action: async () => {
        await AuthService.logout();

        localStorage.removeItem('auth');
        dispatch(setIsAuthorized(false));
        dispatch(setCurrentUser(null));
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className='page logout'>
      <h2>{t('logoutPage.title')}</h2>
    </div>
  );
}
