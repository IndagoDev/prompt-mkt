import './index.scss';

import {
  setCurrentUser,
  setIsAuthorized,
} from '../../../store/slices/userSlice';
import { useEffect, useRef, useState } from 'react';

import { AuthService } from '../../../api/services/authService';
import { Button } from '../../../components/form/button';
import GoogleLogin from 'react-google-login';
import { IRegisterResponse } from '../../../interfaces/IApi';
import { RequestTools } from '../../../tools/requestTools';
import { TextInput } from '../../../components/form/textInput';
import { gapi } from 'gapi-script';
import { useAsyncProcessManagerContext } from '../../../tools/asyncProcessManager/context';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

export function SignUpPage() {
  const dispatch = useDispatch();
  const asyncProcessManager = useAsyncProcessManagerContext();
  const { t } = useTranslation();

  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const [error, setError] = useState<string | null>(null);

  const nameInputRef = useRef<HTMLInputElement>(null);
  const emailInputRef = useRef<HTMLInputElement>(null);
  const passwordInputRef = useRef<HTMLInputElement>(null);

  function onSignUp(result: IRegisterResponse) {
    setError(null);
    localStorage.setItem('auth', result.auth_token);
    dispatch(setIsAuthorized(true));
    dispatch(setCurrentUser(result.user));
  }

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    asyncProcessManager?.doProcess({
      name: 'Do sign up',
      action: async () => {
        try {
          let result = await AuthService.register({
            username: name,
            email,
            password,
          });

          if (result) {
            onSignUp(result);
          } else {
            setError('Unknown error.');
          }
        } catch (error) {
          const message = RequestTools.getApiErrorMessage(error);

          setError(message);
        }
      },
    });
  };

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID,
        scope: 'openid email profile',
      });
    }

    gapi.load('client:auth2', start);
  }, []);

  return (
    <div className='page sign-up'>
      <form onSubmit={handleSubmit}>
        <h1>{t('signUpPage.title')}</h1>
        <TextInput
          id='name'
          label={t('signUpPage.nameLabel')}
          type='text'
          autoComplete='name'
          placeholder={t('signUpPage.namePlaceholder')}
          value={name}
          onChange={(e) => setName(e.target.value)}
          inputRef={nameInputRef}
          required
        />
        <TextInput
          id='email'
          label={t('signUpPage.emailLabel')}
          type='email'
          autoComplete='email'
          placeholder={t('signUpPage.emailPlaceholder')}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          inputRef={emailInputRef}
          required
        />
        <TextInput
          id='password'
          label={t('signUpPage.passwordLabel')}
          type='password'
          autoComplete='new-password'
          placeholder={t('signUpPage.passwordPlaceholder')}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          inputRef={passwordInputRef}
          required
        />
        <div className='buttons-group'>
          <Button type='submit' primary>
            {t('signUpPage.signupButton')}
          </Button>
          <Button isLink={true} linkTo={'/auth/login'}>
            {t('signUpPage.loginButton')}
          </Button>
          <p>{t('signUpPage.or')}</p>
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID ?? ''}
            onSuccess={async (r) => {
              try {
                let apiResponse = await AuthService.onGoogleSignUp(r);
                onSignUp(apiResponse);
              } catch (error) {
                const message = RequestTools.getApiErrorMessage(error);
                setError(message);
              }
            }}
            onFailure={(error) => {
              console.error(error);
            }}
            buttonText={t('signUpPage.signupWithGoogle')}
            cookiePolicy={'single_host_origin'}
            // responseType={"code"}
            accessType={'offline'}
            scope='openid email profile'
          />
        </div>
        <p className='error'>{error}</p>
      </form>
    </div>
  );
}
