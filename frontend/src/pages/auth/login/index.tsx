import './index.scss';

import {
  setCurrentUser,
  setIsAuthorized,
} from '../../../store/slices/userSlice';
import { shopApi, userApi } from '../../../api';
import { useEffect, useRef, useState } from 'react';

import { AuthService } from '../../../api/services/authService';
import { Button } from '../../../components/form/button';
import GoogleLogin from 'react-google-login';
import { ILoginResponse } from '../../../interfaces/IApi';
import { RequestTools } from '../../../tools/requestTools';
import { TextInput } from '../../../components/form/textInput';
import { gapi } from 'gapi-script';
import { useAsyncProcessManagerContext } from '../../../tools/asyncProcessManager/context';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

export function LoginPage() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const asyncProcessManager = useAsyncProcessManagerContext();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const emailInputRef = useRef<HTMLInputElement>(null);
  const passwordInputRef = useRef<HTMLInputElement>(null);

  const [error, setError] = useState<string | null>(null);

  function onLogin(result: ILoginResponse) {
    setError(null);
    localStorage.setItem('auth', result.auth_token);
    dispatch(setIsAuthorized(true));
    dispatch(setCurrentUser(result.user));
  }

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID,
        scope: 'openid email profile',
      });
    }

    gapi.load('client:auth2', start);
  }, []);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    asyncProcessManager?.doProcess({
      name: 'Do login',
      action: async () => {
        try {
          let result = await AuthService.login({
            email,
            password,
          });

          if (result) {
            onLogin(result);
          } else {
            setError(t('loginPage.error.unknown'));
          }
        } catch (error) {
          const code = RequestTools.getApiErrorCode(error);
          const message = RequestTools.getApiErrorMessage(error);

          if (code === 400) {
            setError(t('loginPage.error.invalid'));
          } else {
            setError(message);
          }
        }
      },
    });
  };

  return (
    <div className='page login'>
      <form onSubmit={handleSubmit}>
        <h1>{t('loginPage.title')}</h1>
        <TextInput
          id='email'
          label={t('loginPage.emailLabel')}
          type='email'
          autoComplete='email'
          placeholder={t('loginPage.emailPlaceholder')}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          inputRef={emailInputRef}
          required
        />
        <TextInput
          id='password'
          label={t('loginPage.passwordLabel')}
          type='password'
          autoComplete='current-password'
          placeholder={t('loginPage.passwordPlaceholder')}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          inputRef={passwordInputRef}
          required
        />
        <div className='buttons-group'>
          <Button type='submit' primary>
            {t('loginPage.loginButton')}
          </Button>
          <Button isLink={true} linkTo={'/auth/signup'}>
            {t('loginPage.signupButton')}
          </Button>
          <p>{t('loginPage.or')}</p>
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID ?? ''}
            onSuccess={async (r) => {
              try {
                let apiResponse = await AuthService.onGoogleLogin(r);
                onLogin(apiResponse);
              } catch (error) {
                const message = RequestTools.getApiErrorMessage(error);
                setError(message);
              }
            }}
            onFailure={(error) => {
              console.error(error);
            }}
            buttonText={t('loginPage.loginWithGoogle')}
            cookiePolicy={'single_host_origin'}
            accessType={'offline'}
            scope='openid email profile'
          />
        </div>
        <p className='error'>{error}</p>
      </form>
    </div>
  );
}
