import 'swiper/css';
import 'swiper/css/navigation';
import './index.scss';

import React, { useEffect, useRef, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';

import { BootstrapIcon } from '../../../components/bootstrapIcon';
import { Button } from '../../../components/form/button';
import { IPrompt } from '../../../interfaces/IApi';
import { MarketplaceService } from '../../../api/services/marketplaceService';
import { Navigation } from 'swiper';
import { Pagination } from 'swiper';
import { RequestTools } from '../../../tools/requestTools';
import { RootState } from '../../../store/store';
import { setPromptBuyDialog } from '../../../store/slices/appSlice';
import { useAsyncProcessManagerContext } from '../../../tools/asyncProcessManager/context';
import { useTranslation } from 'react-i18next'; // Импорт хука useTranslation

export function PromptDetails() {
  const asyncProcessManager = useAsyncProcessManagerContext();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { id } = useParams();

  const [prompt, setPrompt] = useState<IPrompt | null>(null);

  const { t } = useTranslation(); // Инициализация хука useTranslation

  const isAuthorized = useSelector(
    (state: RootState) => state.user.isAuthorized
  );
  const currentUser = useSelector((state: RootState) => state.user.currentUser);

  const [isLiked, setIsLiked] = useState<boolean>(prompt?.is_liked ?? false);

  useEffect(() => {
    setIsLiked(prompt?.is_liked ?? false);
  }, [prompt?.is_liked]);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: t('promptPage.promptDetails.fetchPromptDetails'), // Использование ключа перевода
      action: async () => {
        try {
          let newPrompt = await MarketplaceService.getPrompt({
            id: id ? +id : 0,
          });

          setPrompt(newPrompt);
        } catch (error) {
          const code = RequestTools.getApiErrorCode(error);

          if (code === 404) {
            navigate('/marketplace');
          }
        }
      },
    });
  }, [id]);

  function handleOnBuy(e: React.MouseEvent<HTMLButtonElement>) {
    e.preventDefault();

    if (!prompt) return;

    dispatch(
      setPromptBuyDialog({
        active: true,
        id: prompt.id,
        creator: prompt.user ?? 0,
        amount: +prompt.price ?? 0,
        currency: '$',
      })
    );
  }

  return (
    <div className='page prompt'>
      <div className='flex fade-in'>
        <div className='swiper-wrapper'>
          <Swiper navigation={true} modules={[Navigation]} className='mySwiper'>
            {prompt?.attachments?.map((attachment, index) => {
              return (
                <SwiperSlide key={index}>
                  <img src={attachment._file} alt='' />
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>

        <div className='info'>
          <h1>{prompt?.name}</h1>
          <code>{prompt?.description}</code>
          <p>
            <b>{t('promptPage.promptDetails.price')}</b>
            {prompt?.price}
          </p>
          <p>
            <b>{t('promptPage.promptDetails.exampleInput')}</b>
            {prompt?.example_input}
          </p>
          <p>
            <b>{t('promptPage.promptDetails.exampleOutput')}</b>
            {prompt?.example_output}
          </p>
          <p>
            <b>{t('promptPage.promptDetails.creationDate')}</b>
            {prompt?.creation_date
              ? new Date(prompt.creation_date).toLocaleDateString('uk-ua')
              : ''}
          </p>
          <p>
            <b>{t('promptPage.promptDetails.instructions')}</b>
            {prompt?.instructions}
          </p>
          {prompt?.prompt_template && (
            <p>
              <b>{t('promptPage.promptDetails.promptTemplate')}</b>
              {prompt?.prompt_template}
            </p>
          )}
          <p>
            <b>{t('promptPage.promptDetails.categories')}</b>
            {prompt?.categories?.map((c) => c.name)?.join(', ')}
          </p>
          <div className='actions'>
            {!prompt?.purchased && (
              <Button
                onClick={handleOnBuy}
                primary={true}
                icon='cart'
                text={prompt?.price + '$'}
              />
            )}
            {isAuthorized && (
              <button
                className='like-button'
                onClick={(e) => {
                  e.preventDefault();

                  asyncProcessManager?.doProcess({
                    name: t('promptPage.actions.toggleLike'), // Использование ключа перевода
                    action: async () => {
                      if (isLiked) {
                        await MarketplaceService.deleteLikePrompt({
                          promptId: prompt?.id ?? 0,
                        });
                      } else {
                        await MarketplaceService.likePrompt({
                          sender: currentUser?.id ?? 0,
                          receiver: prompt?.id ?? 0,
                        });
                      }

                      setIsLiked(!isLiked);
                    },
                  });
                }}
              >
                <BootstrapIcon icon={isLiked ? 'heart-fill' : 'heart'} />
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
