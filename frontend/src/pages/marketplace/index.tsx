import './index.scss';

import { ChangeEvent, useEffect, useState } from 'react';
import { ICategory, IGetPromptsResponse, ITag } from '../../interfaces/IApi';
import { useNavigate, useSearchParams } from 'react-router-dom';

import { Checkbox } from '../../components/checkbox';
import { ItemsSwitch } from '../../components/itemsSwitch';
import { MarketplaceService } from '../../api/services/marketplaceService';
import { ProductCard } from '../../components/productCard';
import { RootState } from '../../store/store';
import { useAsyncProcessManagerContext } from '../../tools/asyncProcessManager/context';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export function MarketplacePage() {
  const asyncProcessManager = useAsyncProcessManagerContext();

  const searchQuery = useSelector((state: RootState) => state.app.searchQuery);

  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const { t } = useTranslation();

  const [tags, setTags] = useState<ITag[]>([]);
  const [categories, setCategories] = useState<ICategory[]>([]);
  const [modelCategories, setModelCategories] = useState<ICategory[]>([]);
  const [prompts, setPrompts] = useState<IGetPromptsResponse | null>(null);

  const [selectedTags, setSelectedTags] = useState<number[]>([]);
  const [selectedCategories, setSelectedCategories] = useState<number[]>([]);
  const [selectedModelCategories, setSelectedModelCategories] = useState<
    number[]
  >([]);
  const [selectedSort, setSelectedSort] = useState<string>('');

  useEffect(() => {
    // При загрузке компонента считываем параметры из URL и устанавливаем соответствующие значения выбранных элементов
    const selectedTagIds = searchParams.getAll('tags').map(Number);
    const selectedCategoryIds = searchParams.getAll('categories').map(Number);
    const selectedModelCategoryIds = searchParams
      .getAll('modelCategories')
      .map(Number);

    setSelectedTags(selectedTagIds);
    setSelectedCategories(selectedCategoryIds);
    setSelectedModelCategories(selectedModelCategoryIds);
    setSelectedSort(searchParams.get('marketplace.sort') ?? 'sell_amount');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleCheckboxChange = (
    key: 'tag' | 'category' | 'modelCategory',
    item: ITag | ICategory,
    isChecked: boolean
  ) => {
    const selectedCopy = isChecked
      ? [
          ...(key === 'tag'
            ? selectedTags
            : key === 'category'
            ? selectedCategories
            : selectedModelCategories),
          item.id,
        ]
      : key === 'tag'
      ? selectedTags.filter((selectedId) => selectedId !== item.id)
      : key === 'category'
      ? selectedCategories.filter((selectedId) => selectedId !== item.id)
      : selectedModelCategories.filter((selectedId) => selectedId !== item.id);

    if (key === 'tag') {
      setSelectedTags(selectedCopy);
    } else if (key === 'category') {
      setSelectedCategories(selectedCopy);
    } else {
      setSelectedModelCategories(selectedCopy);
    }

    // При изменении выбранных элементов обновляем параметры запроса в URL
    searchParams.delete(key);
    selectedCopy.forEach((selectedId) =>
      searchParams.append(key, String(selectedId))
    );

    navigate(`?${searchParams.toString()}`, { replace: true });
  };

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch tags',
      action: async () => {
        let newTags = await MarketplaceService.getTags();
        setTags(newTags);
      },
    });

    asyncProcessManager?.doProcess({
      name: 'Fetch categories',
      action: async () => {
        let newCategories = await MarketplaceService.getCategories();
        setCategories(newCategories);
      },
    });

    asyncProcessManager?.doProcess({
      name: 'Fetch model categories',
      action: async () => {
        let newCategories = await MarketplaceService.getModelCategories();
        setModelCategories(newCategories);
      },
    });
  }, [asyncProcessManager]);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch prompts',
      action: async () => {
        let newPrompts = await MarketplaceService.getPrompts({
          tags: selectedTags,
          categories: selectedCategories,
          model_categories: selectedModelCategories,
          name: searchQuery,
          sort_by: selectedSort,
        });
        setPrompts(newPrompts);
      },
    });
  }, [
    selectedTags,
    selectedCategories,
    selectedModelCategories,
    searchQuery,
    selectedSort,
    asyncProcessManager,
  ]);

  return (
    <div className='page marketplace'>
      <aside className='sidebar'>
        <div className='filter-group'>
          <h5>{t('marketplace.sidebar.sorting')}</h5>
          <ItemsSwitch
            vertical={true}
            value={selectedSort}
            onChange={(value) => {
              let stringValue = value ? value.toString() : '';
              setSelectedSort(stringValue);
              searchParams.set('marketplace.sort', stringValue);

              navigate(`?${searchParams.toString()}`, { replace: true });
            }}
            items={[
              {
                icon: 'basket',
                title: t('marketplace.sidebar.bySellAmount'),
                value: 'sell_amount',
              },
              {
                icon: 'bar-chart',
                title: t('marketplace.sidebar.byAverageRating'),
                value: 'average_rating',
              },
              {
                icon: 'calendar2-check',
                title: t('marketplace.sidebar.byCreationDate'),
                value: 'creation_date',
              },
            ]}
          />
        </div>
        <div className='filter-group'>
          <h5>{t('marketplace.sidebar.tags')}</h5>
          <div className='options'>
            {tags.map((tag, index) => {
              return (
                <Checkbox
                  checked={selectedTags.includes(tag.id)}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    handleCheckboxChange('tag', tag, e.target.checked)
                  }
                  key={index}
                  title={tag.name}
                />
              );
            })}
          </div>
        </div>
        <div className='filter-group'>
          <h5>{t('marketplace.sidebar.category')}</h5>
          <div className='options'>
            {categories.map((category, index) => {
              return (
                <Checkbox
                  checked={selectedCategories.includes(category.id)}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    handleCheckboxChange('category', category, e.target.checked)
                  }
                  key={index}
                  title={category.name}
                />
              );
            })}
          </div>
        </div>
        <div className='filter-group'>
          <h5>{t('marketplace.sidebar.modelCategory')}</h5>
          <div className='options'>
            {modelCategories.map((category, index) => {
              return (
                <Checkbox
                  checked={selectedModelCategories.includes(category.id)}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    handleCheckboxChange(
                      'modelCategory',
                      category,
                      e.target.checked
                    )
                  }
                  key={index}
                  title={category.name}
                />
              );
            })}
          </div>
        </div>
      </aside>
      <section className='cards'>
        {prompts?.results.map((prompt, index) => {
          return (
            <ProductCard
              id={prompt.id}
              creator={prompt.user ?? 0}
              key={index}
              category={prompt.model_category.name}
              name={prompt.name}
              price={prompt.price}
              image={prompt.image}
              is_liked={prompt.is_liked ?? false}
              paid={prompt.purchased ?? false}
            />
          );
        })}

        {(!prompts || prompts.results.length === 0) && (
          <p>{t('marketplace.prompts.notFound')}</p>
        )}
      </section>
    </div>
  );
}
