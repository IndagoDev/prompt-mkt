import './index.scss';

export function NotFoundPage() {
  return (
    <div className='page not-found'>
      <h1>404</h1>
    </div>
  );
}
