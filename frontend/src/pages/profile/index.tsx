import './index.scss';

import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import { BootstrapIcon } from '../../components/bootstrapIcon';
import { Button } from '../../components/form/button';
import { IUser } from '../../interfaces/IApi';
import { ProductCard } from '../../components/productCard';
import { RequestTools } from '../../tools/requestTools';
import { RootState } from '../../store/store';
import { UserService } from '../../api/services/userService';
import { useAsyncProcessManagerContext } from '../../tools/asyncProcessManager/context';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export default function CurrentUserProfile() {
  const navigate = useNavigate();
  const asyncProcessManager = useAsyncProcessManagerContext();
  const { username } = useParams();

  const [user, setUser] = useState<IUser | null>(null);
  const currentUser = useSelector((state: RootState) => state.user.currentUser);

  const { t } = useTranslation();

  function toggleSubscribe() {
    asyncProcessManager?.doProcess({
      name: `Toggle subscription to ${username}`,
      action: async () => {
        if (!currentUser?.id || !user?.id) return;

        try {
          if (user.is_in_subscriptions) {
            await UserService.unsubscribe({
              username: user.username,
            });

            user.is_in_subscriptions = false;
          } else {
            await UserService.subscribe({
              sender: currentUser.id,
              receiver: user.id,
            });

            user.is_in_subscriptions = true;
          }
        } catch (error) {
          const code = RequestTools.getApiErrorCode(error);

          if (code === 404) {
            navigate('/profile');
          } else {
            throw error;
          }
        }
      },
    });
  }

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch user',
      action: async () => {
        try {
          let response = await UserService.getUser({
            username: username ?? '',
          });

          UserService.updateUserLookups({
            username: username ?? '',
          });

          if (response) {
            console.log(response);
            setUser(response);
          }
        } catch (error) {
          const code = RequestTools.getApiErrorCode(error);

          if (code === 404) {
            navigate('/profile');
          } else {
            throw error;
          }
        }
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className='page profile'>
      <div className='profile-images'>
        <div
          className='banner'
          style={{
            backgroundImage: user?.background_photo
              ? `url(${user?.background_photo})`
              : undefined,
          }}
        ></div>
        <div
          className='avatar'
          style={{
            backgroundImage: user?.avatar ? `url(${user?.avatar})` : undefined,
          }}
        ></div>
      </div>
      <div className='profile-info'>
        <h4>@{user?.username}</h4>
        <div className='stats'>
          <div className='stat'>
            <BootstrapIcon icon='eye-fill' />
            <span>{user?.amount_of_lookups ?? 0}</span>
          </div>
          <div className='stat'>
            <BootstrapIcon icon='heart-fill' />
            <span>{user?.amount_of_likes ?? 0}</span>
          </div>
          <div className='stat'>
            <BootstrapIcon icon='tag-fill' />
            <span>{user?.amount_of_sells ?? 0}</span>
          </div>
        </div>
        <Button
          primary={!user?.is_in_subscriptions}
          text={user?.is_in_subscriptions ? t('profile.unsubscribe') : t('profile.subscribe')}
          onClick={(e) => {
            e.preventDefault();
            toggleSubscribe();
          }}
          isLink={false}
        />
      </div>

      <div className='profile-products'>
        <h2>{t('profile.mostPopularPrompts')}</h2>
        <br />
        <div className='cards-wrapper'>
          {(user?.most_popular_prompts ?? []).map((prompt, index) => {
            return (
              <ProductCard
                id={prompt.id}
                creator={prompt.user ?? 0}
                key={index}
                category={prompt.model_category.name}
                name={prompt.name}
                price={prompt.price}
                image={prompt.image}
                is_liked={prompt.is_liked ?? false}
                paid={prompt.purchased ?? false}
              />
            );
          })}
        </div>
        <Button
          iconPosition='right'
          text={t('profile.browseMarketplace')}
          icon='arrow-up-right'
        />
      </div>

      <div className='profile-products'>
        <h2>{t('profile.newestPrompts')}</h2>
        <br />
        <div className='cards-wrapper'>
          {(user?.newest_prompts ?? []).map((prompt, index) => {
            return (
              <ProductCard
                id={prompt.id}
                creator={prompt.user ?? 0}
                key={index}
                category={prompt.model_category.name}
                name={prompt.name}
                price={prompt.price}
                image={prompt.image}
                is_liked={prompt.is_liked ?? false}
                paid={prompt.purchased ?? false}
              />
            );
          })}
        </div>
        <Button
          iconPosition='right'
          text={t('profile.browseMarketplace')}
          icon='arrow-up-right'
        />
      </div>
    </div>
  );
}
