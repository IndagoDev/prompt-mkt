import './index.scss';

import { useTranslation } from 'react-i18next';

export function Contact() {
  const { t } = useTranslation();

  return (
    <div className='page contact'>
      <h1>{t('contactPage.title')}</h1>
      <p>{t('contactPage.description')}</p>
      <p>
        {t('contactPage.email')}{' '}
        <a href='mailto:hello@prompt-mkt.com'>{t('contactPage.emailLink')}</a>
      </p>
      <p>
        {t('contactPage.stripeInfo')}{' '}
        <a href='https://twitter.com/promptmkt'>
          {t('contactPage.twitterLink')}
        </a>
        . {t('contactPage.countryNotifications')}{' '}
        <a href='#'>{t('contactPage.countryNotificationsLink')}</a>{' '}
        {t('contactPage.countryNotificationsLinkText')}
      </p>
      <p>{t('contactPage.closing')}</p>
    </div>
  );
}
