import './index.scss';

import { Accordion } from '../../components/accordion';
import { AccordionItem } from '../../components/accordion/item';
import { useTranslation } from 'react-i18next';

export function FAQ() {
  const { t } = useTranslation();

  return (
    <div className='page faq'>
      <h1>{t('faq.pageTitle')}</h1>
      <Accordion>
        {(t('faq.accordionItems', { returnObjects: true }) as any)?.map(
          (item: any, index: number) => (
            <AccordionItem
              key={index}
              expanded={false}
              icon='diamond'
              body={<>{item.body}</>}
              title={item.title}
            />
          )
        )}
      </Accordion>
    </div>
  );
}
