import './index.scss';

import {
  IMainDataGPT,
  IMainDataVisual,
} from '../../interfaces/IPromptCreation';
import { useEffect, useState } from 'react';

import { Button } from '../../components/form/button';
import { InitialDataStep } from './steps/initialData';
import { MarketplaceService } from '../../api/services/marketplaceService';
import { PromptFileStep } from './steps/promptFile';
import { RootState } from '../../store/store';
import { useAsyncProcessManagerContext } from '../../tools/asyncProcessManager/context';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

export function SellPage() {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const asyncProcessManager = useAsyncProcessManagerContext();

  const currentUser = useSelector(
    (state: RootState) => state.user?.currentUser
  );

  const [step, setStep] = useState(1);

  const [initialData, setInitialData] = useState({
    promptType: null,
    name: '',
    description: '',
    estimatedPrice: null,
    exampleInput: '',
    exampleOutput: '',
    selectedCategories: [],
    selectedTags: [],
  });
  const [mainData, setMainData] = useState({
    prompt: '',
    promptInstructions: '',
    customFields: {},
  });

  useEffect(() => {
    console.log(JSON.stringify({ initialData, mainData }, null, 2));
  }, [initialData, mainData]);

  const allStepsCount = 2;

  const stepsProgress = (step / allStepsCount) * 100;

  function prevStep() {
    let newStep = step - 1;

    if (newStep < 1) {
      newStep = 1;
    }

    setStep(newStep);
  }

  function nextStep() {
    let newStep = step + 1;

    if (newStep > allStepsCount) {
      newStep = allStepsCount;
    }

    setStep(newStep);
  }

  function submit() {
    asyncProcessManager?.doProcess({
      name: t('sellPage.createPrompt'),
      action: async () => {
        if (!initialData.promptType || !currentUser) return;

        try {
          let updatedAttachmentsPending = (
            (mainData as IMainDataVisual).attachments ?? []
          ).map(async (a) => {
            let newA = { ...a };

            if (!newA.apiId) {
              let createAttachmentResult =
                await MarketplaceService.createAttachment({
                  file_type: 1,
                  _file: a.file,
                });

              newA.apiId = createAttachmentResult.id;
            }

            return newA;
          });

          let updatedAttachments = await Promise.all(updatedAttachmentsPending);

          let response = await MarketplaceService.createPrompt({
            model_category: initialData.promptType,
            name: initialData.name ?? '',
            image: (mainData as IMainDataVisual)?.attachments[0]?.file ?? null,
            token_size: '32',
            description: initialData.description,
            example_input: initialData.exampleInput,
            user: currentUser.id,
            example_output: initialData.exampleOutput,
            prompt_template: mainData.prompt,
            instructions: mainData.promptInstructions,
            categories: initialData.selectedCategories ?? [],
            tags: initialData.selectedTags ?? [],
            sell_amount: +(initialData.estimatedPrice ?? 0),
            attachments:
              (updatedAttachments
                ?.map((a) => a.apiId)
                ?.filter((a) => a !== undefined) as number[]) ?? [],
            custom_parameters: JSON.stringify(mainData.customFields),
          });

          if (response) {
            navigate(`/marketplace/${response.id}`);
          }
        } catch (error) {
          throw error;
        }
      },
    });
  }

  return (
    <div className='page sell'>
      <div className='progress-wrapper'>
        <Button
          onClick={() => prevStep()}
          size={'mini'}
          icon='chevron-left'
          text={t('sellPage.back')}
        />
        <div className='progress-bar-wrapper'>
          <div
            className='progress-bar'
            style={{ width: `${stepsProgress}%` }}
          ></div>
        </div>
        <div className='steps-wrapper'>
          <div>
            {t('sellPage.step')} {step}/{allStepsCount}
          </div>
        </div>
        <div className='delete-prompt-wrapper'></div>
      </div>
      {step === 1 && (
        <InitialDataStep
          initialData={initialData}
          setInitialData={setInitialData}
          onNextStep={() => nextStep()}
        />
      )}
      {step === 2 && (
        <PromptFileStep
          promptType={initialData.promptType}
          initialData={initialData}
          mainData={mainData}
          setMainData={setMainData}
          onNextStep={() => submit()}
        />
      )}
    </div>
  );
}
