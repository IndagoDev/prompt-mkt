import {
  IInitialData,
  IMainDataGeneric,
  IMainDataVisual,
} from '../../../../interfaces/IPromptCreation';
import { useEffect, useState } from 'react';

import { Button } from '../../../../components/form/button';
import { ICustomParameters } from '../../../../interfaces/IApi';
import { ImageUploader } from './components/imageUploader';
import { MarketplaceService } from '../../../../api/services/marketplaceService';
import { TextArea } from '../../../../components/form/textArea';
import { TextInput } from '../../../../components/form/textInput';
import i18next from 'i18next';
import { useAsyncProcessManagerContext } from '../../../../tools/asyncProcessManager/context';
import { useTranslation } from 'react-i18next';

export interface IPromptFileStep {
  promptType: number | null;
  initialData: IInitialData;
  mainData: IMainDataGeneric;
  setMainData: (data: IMainDataGeneric) => void;
  onNextStep: () => void;
}

export function PromptFileStep(props: IPromptFileStep) {
  const { t } = useTranslation();

  const asyncProcessManager = useAsyncProcessManagerContext();

  const [customParameters, setCustomParameters] =
    useState<ICustomParameters | null>(null);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch custom fields',
      action: async () => {
        let result = await MarketplaceService.getCustomParameters({
          id: props.promptType ?? 0,
        });

        if (result) {
          setCustomParameters(result.custom_parameters);
        }
      },
    });
  }, [props.promptType]);

  return (
    <div className='fade-in signup-step'>
      <div className='two-col'>
        <div className='left-col'>
          <h2>{t('promptFile.promptContent')}</h2>
          <div className='fields'>
            {customParameters &&
              customParameters?.fields &&
              customParameters.fields.map((field) => {
                let fieldValue =
                  field.name in props.mainData.customFields
                    ? props.mainData.customFields[field.name]
                    : '';

                return (
                  <div className='field'>
                    <TextInput
                      value={fieldValue}
                      onChange={(e) =>
                        props.setMainData({
                          ...props.mainData,
                          customFields: {
                            [field.name]: e.target.value,
                          },
                        })
                      }
                      placeholder={field.placeholder}
                      label={i18next.language === 'en' ? field.label_en : field.label_ua}
                      type={'text'}
                    />
                  </div>
                );
              })}
            <div className='field'>
              <TextArea
                value={props.mainData.prompt}
                onChange={(e) =>
                  props.setMainData({
                    ...props.mainData,
                    prompt: e.target.value,
                  })
                }
                placeholder={t('promptFile.promptContent')}
                label={t('promptFile.promptContent')}
              />
            </div>
            <div className='field'>
              <TextArea
                value={props.mainData.promptInstructions}
                onChange={(e) =>
                  props.setMainData({
                    ...props.mainData,
                    promptInstructions: e.target.value,
                  })
                }
                placeholder={t('promptFile.promptInstructions')}
                label={t('promptFile.promptInstructions')}
                rows={6}
              />
            </div>
            <div className='field'>
              <ImageUploader
                images={(props.mainData as IMainDataVisual).attachments ?? []}
                onChange={(images) => {
                  console.log(images);
                  props.setMainData({
                    ...props.mainData,
                    attachments: images,
                  });
                }}
              />
            </div>

            <Button
              onClick={() => props.onNextStep()}
              text={t('promptFile.next')}
              icon='chevron-right'
              iconPosition='right'
            />
          </div>
        </div>
      </div>
    </div>
  );
}
