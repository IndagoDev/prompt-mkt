import './index.scss';

import { ChangeEvent, useId, useState } from 'react';

import { BootstrapIcon } from '../../../../../../components/bootstrapIcon';
import { IAttachment } from '../../../../../../interfaces/IPromptCreation';
import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';

export interface ImageUploaderProps {
  images: IAttachment[];
  maxImages?: number;
  onChange?: (images: IAttachment[]) => void;
}

export function ImageUploader(props: ImageUploaderProps) {
  const { images, maxImages, onChange } = props;

  const id = useId();
  const { t } = useTranslation();

  const [selectedImages, setSelectedImages] = useState<IAttachment[]>(images);

  const handleImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;

    if (files) {
      let newImages: File[] = Array.from(files);

      if (maxImages) {
        const remainingSpaces = maxImages - selectedImages.length;
        if (remainingSpaces < newImages.length) {
          newImages = newImages.slice(0, remainingSpaces);
          toast.error(t('imageUploader.errorMessage', { remainingSpaces }));
        }
      }

      newImages.forEach((file) => {
        const f = file;
        const reader = new FileReader();
        reader.onloadend = () => {
          const newImage = {
            id: Math.random().toString(36).substr(2, 9),
            src: reader.result as string,
            file: f,
            apiId: undefined,
          };
          setSelectedImages((prevImages) => {
            const updatedImages = [...prevImages, newImage];
            onChange?.(updatedImages);
            return updatedImages;
          });
        };
        reader.readAsDataURL(file);
      });
    }
  };

  const handleDeleteImage = (id: string) => {
    setSelectedImages((prevImages) => {
      const updatedImages = prevImages.filter((image) => image.id !== id);
      onChange?.(updatedImages);
      return updatedImages;
    });
  };

  return (
    <div className='smart-image-uploader'>
      <input
        type='file'
        accept='image/jpeg, image/png'
        multiple
        onChange={handleImageChange}
        style={{ display: 'none' }}
        id={id}
      />
      {selectedImages.map((image) => (
        <div className='fade-in selected-image' key={image.id}>
          <div
            className='image'
            style={{ backgroundImage: `url(${image.src})` }}
          ></div>
          <button onClick={() => handleDeleteImage(image.id)}>
            <BootstrapIcon icon='x-lg' />
          </button>
        </div>
      ))}
      {selectedImages.length < (maxImages ?? 100) && (
        <label
          className='fade-in add-button'
          htmlFor={id}
          style={{ cursor: 'pointer' }}
        >
          <BootstrapIcon icon='plus-lg' />
        </label>
      )}
    </div>
  );
}
