import './index.scss';

import { ICategory, ITag } from '../../../../interfaces/IApi';
import { WithContext as ReactTags, Tag } from 'react-tag-input';
import { useEffect, useState } from 'react';

import { Button } from '../../../../components/form/button';
import { IInitialData } from '../../../../interfaces/IPromptCreation';
import { MarketplaceService } from '../../../../api/services/marketplaceService';
import { Select } from '../../../../components/form/select';
import { TextArea } from '../../../../components/form/textArea';
import { TextInput } from '../../../../components/form/textInput';
import { useAsyncProcessManagerContext } from '../../../../tools/asyncProcessManager/context';
import { useTranslation } from 'react-i18next';

export interface IInitialDataStep {
  initialData: IInitialData;
  setInitialData: (initialData: IInitialData) => void;
  onNextStep: () => void;
}

export function InitialDataStep(props: IInitialDataStep) {
  const asyncProcessManager = useAsyncProcessManagerContext();

  const [tags, setTags] = useState<ITag[]>([]);
  const [categories, setCategories] = useState<ICategory[]>([]);
  const [modelCategories, setModelCategories] = useState<ICategory[]>([]);

  const [selectedTags, setSelectedTags] = useState<Tag[]>([]);
  const [selectedCategories, setSelectedCategories] = useState<Tag[]>([]);

  const [formSubmitted, setFormSubmitted] = useState<boolean>(false);

  const { t } = useTranslation();

  useEffect(() => {
    props.setInitialData({
      ...props.initialData,
      selectedTags: selectedTags.map((t) => +t.id),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTags]);

  useEffect(() => {
    props.setInitialData({
      ...props.initialData,
      selectedCategories: selectedCategories.map((t) => +t.id),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCategories]);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch tags',
      action: async () => {
        let newTags = await MarketplaceService.getTags();
        setTags(newTags);

        let newSelectedTags: Tag[] = newTags
          ? newTags
              ?.map(
                (t) =>
                  ({
                    id: t.id.toString(),
                    text: t.name,
                  } as Tag)
              )
              .filter((tt) => props.initialData.selectedTags.includes(+tt.id))
          : [];

        setSelectedTags(newSelectedTags);
      },
    });

    asyncProcessManager?.doProcess({
      name: 'Fetch categories',
      action: async () => {
        let newCategories = await MarketplaceService.getCategories();
        setCategories(newCategories);

        let newSelectedCategories: Tag[] = newCategories
          ? newCategories
              ?.map(
                (c) =>
                  ({
                    id: c.id.toString(),
                    text: c.name,
                  } as Tag)
              )
              .filter((ct) =>
                props.initialData.selectedCategories.includes(+ct.id)
              )
          : [];

        setSelectedCategories(newSelectedCategories);
      },
    });

    asyncProcessManager?.doProcess({
      name: 'Fetch model categories',
      action: async () => {
        let newCategories = await MarketplaceService.getModelCategories();
        setModelCategories(newCategories);
      },
    });
  }, [asyncProcessManager]);

  const suggestionsTags: Tag[] = tags.map((tag) => {
    return {
      id: tag.id.toString(),
      text: tag.name,
    };
  });

  const suggestionsCategories: Tag[] = categories.map((category) => {
    return {
      id: category.id.toString(),
      text: category.name,
    };
  });

  const KeyCodes = {
    comma: 188,
    enter: 13,
  };

  const delimiters = [KeyCodes.comma, KeyCodes.enter];

  const handleTagDelete = (i: number) => {
    setSelectedTags(selectedTags.filter((_tag, index) => index !== i));
  };

  const handleTagAddition = (tag: Tag) => {
    if (tags.map((t) => t.name).includes(tag.text)) {
      setSelectedTags([...selectedTags, tag]);
    } else {
      asyncProcessManager?.doProcess({
        name: 'Create tag',
        action: async () => {
          let result = await MarketplaceService.createTag({
            is_system: false,
            name: tag.text,
          });

          if (result) {
            setSelectedTags([
              ...selectedTags,
              {
                id: result.id.toString(),
                text: result.name,
              },
            ]);
          }
        },
      });
    }
  };

  const handleTagDrag = (tag: Tag, currPos: number, newPos: number) => {
    const newTags = selectedTags.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    setSelectedTags(newTags);
  };

  const handleCategoryDelete = (i: number) => {
    setSelectedCategories(
      selectedCategories.filter((_tag, index) => index !== i)
    );
  };

  const handleCategoryAddition = (tag: Tag) => {
    if (categories.map((t) => t.name).includes(tag.text)) {
      setSelectedCategories([...selectedCategories, tag]);
    }
  };

  const handleCategoryDrag = (tag: Tag, currPos: number, newPos: number) => {
    const newTags = selectedCategories.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    setSelectedCategories(newTags);
  };

  const isValid =
    props.initialData.description !== '' &&
    props.initialData.estimatedPrice !== '' &&
    props.initialData.exampleInput !== '' &&
    props.initialData.exampleOutput !== '' &&
    props.initialData.name !== '' &&
    props.initialData.promptType !== null &&
    props.initialData.promptType > 0 &&
    props.initialData.selectedCategories.length > 0 &&
    props.initialData.selectedTags.length > 0;

  return (
    <div className='fade-in signup-step'>
      <div className='two-col'>
        <div className='left-col'>
          <h2>{t('initialDataStep.promptDetails')}</h2>
          <div className='prompt-info'>
            <p>{t('initialDataStep.tellUsPrompt')}</p>
            <p>{t('initialDataStep.detailsNotFinal')}</p>
          </div>

          <div className='fields'>
            <div className='field'>
              <div className='field-title'>
                {t('initialDataStep.promptType')}
              </div>
              <div className='field-help'>
                <i>{t('initialDataStep.selectPromptType')}</i>
              </div>
              <Select
                required={true}
                isValid={!formSubmitted || props.initialData.promptType > 0}
                validationError={t('initialDataStep.selectPromptType')}
                onChange={(e) =>
                  props.setInitialData({
                    ...props.initialData,
                    promptType: +e.target.value,
                  })
                }
              >
                <option
                  selected={props.initialData.promptType === null}
                  disabled
                >
                  {t('initialDataStep.selectPromptType')}
                </option>
                {modelCategories.map((category, index) => (
                  <option
                    selected={category.id === props.initialData.promptType}
                    key={index}
                    value={category.id}
                  >
                    {category.name}
                  </option>
                ))}
              </Select>
            </div>
            <div className='field'>
              <div className='field-title'>{t('initialDataStep.name')}</div>
              <div className='field-help'>
                <i>{t('initialDataStep.suggestTitle')}</i>
              </div>
              <TextInput
                type='text'
                maxLength={40}
                className='field-value'
                placeholder={t('initialDataStep.enterTitle')}
                value={props.initialData.name}
                required={true}
                validationError={t('initialDataStep.titleRequired')}
                isValid={!formSubmitted || props.initialData.name.length > 0}
                onChange={(e) => {
                  props.setInitialData({
                    ...props.initialData,
                    name: e.target.value,
                  });
                }}
              />
            </div>
            <div className='field'>
              <div className='field-title'>
                {t('initialDataStep.description')}
              </div>
              <div className='field-help'>
                <i>{t('initialDataStep.describePrompt')}</i>
              </div>
              <TextArea
                maxLength={500}
                className='prompt-description draggable'
                placeholder={t('initialDataStep.enterDescription')}
                value={props.initialData.description}
                required={true}
                validationError={t('initialDataStep.descriptionRequired')}
                isValid={
                  !formSubmitted || props.initialData.description.length > 0
                }
                onChange={(e) => {
                  props.setInitialData({
                    ...props.initialData,
                    description: e.target.value,
                  });
                }}
              ></TextArea>
            </div>
            <div className='field'>
              <div className='field-title'>
                {t('initialDataStep.exampleInput')}
              </div>
              <TextArea
                maxLength={500}
                className='example-input draggable'
                placeholder={t('initialDataStep.enterExampleInput')}
                value={props.initialData.exampleInput}
                required={true}
                validationError={t('initialDataStep.exampleInputRequired')}
                isValid={
                  !formSubmitted || props.initialData.exampleInput.length > 0
                }
                onChange={(e) => {
                  props.setInitialData({
                    ...props.initialData,
                    exampleInput: e.target.value,
                  });
                }}
              ></TextArea>
            </div>
            <div className='field'>
              <div className='field-title'>
                {t('initialDataStep.exampleOutput')}
              </div>
              <TextArea
                maxLength={500}
                className='example-input draggable'
                placeholder={t('initialDataStep.enterExampleOutput')}
                value={props.initialData.exampleOutput}
                required={true}
                validationError={t('initialDataStep.exampleOutputRequired')}
                isValid={
                  !formSubmitted || props.initialData.exampleOutput.length > 0
                }
                onChange={(e) => {
                  props.setInitialData({
                    ...props.initialData,
                    exampleOutput: e.target.value,
                  });
                }}
              ></TextArea>
            </div>

            <div className='field'>
              <div className='field-title'>{t('initialDataStep.tags')}</div>
              <ReactTags
                tags={selectedTags}
                suggestions={suggestionsTags}
                delimiters={delimiters}
                handleDelete={handleTagDelete}
                handleAddition={handleTagAddition}
                handleDrag={handleTagDrag}
                inputFieldPosition='bottom'
                placeholder={t('initialDataStep.searchTags')}
                autocomplete
              />
            </div>
            <div className='field'>
              <div className='field-title'>
                {t('initialDataStep.categories')}
              </div>
              <ReactTags
                tags={selectedCategories}
                suggestions={suggestionsCategories}
                delimiters={delimiters}
                handleDelete={handleCategoryDelete}
                handleAddition={handleCategoryAddition}
                handleDrag={handleCategoryDrag}
                inputFieldPosition='bottom'
                placeholder={t('initialDataStep.searchCategories')}
                autocomplete
              />
            </div>
            <div className='field'>
              <div className='field-title'>
                {t('initialDataStep.estimatedPrice')}
              </div>
              <div className='field-help'>
                <i>{t('initialDataStep.promptPrice')}</i>
              </div>
              <Select
                onChange={(e) => {
                  props.setInitialData({
                    ...props.initialData,
                    estimatedPrice: e.target.value,
                  });
                }}
                required={true}
                validationError={t('initialDataStep.priceRequired')}
                isValid={
                  !formSubmitted ||
                  (props.initialData.estimatedPrice?.length ?? 0) > 0
                }
              >
                <option
                  selected={props.initialData.estimatedPrice === null}
                  disabled
                >
                  {t('initialDataStep.selectPrice')}
                </option>
                {Array(28)
                  .fill(0)
                  .map((_, index) => {
                    let value = (index + 2.99).toFixed(2);
                    return (
                      <option
                        selected={props.initialData.estimatedPrice === value}
                        value={value}
                      >
                        ${value}
                      </option>
                    );
                  })}
              </Select>
            </div>
          </div>
          <br />
          <br />
          <Button
            onClick={() => {
              setFormSubmitted(true);

              if (isValid) props.onNextStep();
            }}
            primary={isValid}
            text={t('common.next')}
            icon='chevron-right'
            iconPosition='right'
          />
        </div>
        <div className='right-col center-text'></div>
      </div>
    </div>
  );
}
