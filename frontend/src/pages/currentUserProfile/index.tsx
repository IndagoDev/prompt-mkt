import './index.scss';

import { setCurrentUser, setIsAuthorized } from '../../store/slices/userSlice';
import { useDispatch, useSelector } from 'react-redux';

import { BootstrapIcon } from '../../components/bootstrapIcon';
import { Button } from '../../components/form/button';
import { Link } from 'react-router-dom';
import { ProductCard } from '../../components/productCard';
import { RootState } from '../../store/store';
import { UserService } from '../../api/services/userService';
import { useAsyncProcessManagerContext } from '../../tools/asyncProcessManager/context';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

export default function CurrentUserProfile() {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const asyncProcessManager = useAsyncProcessManagerContext();
  const currentUser = useSelector((state: RootState) => state.user.currentUser);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch current user',
      action: async () => {
        let response = await UserService.getCurrentUser();

        if (response) {
          console.log(response);
          dispatch(setCurrentUser(response));
          dispatch(setIsAuthorized(true));
        }
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className='page profile'>
      <div className='profile-images'>
        <div
          className='banner'
          style={{
            backgroundImage: currentUser?.background_photo
              ? `url(${currentUser?.background_photo})`
              : undefined,
          }}
        ></div>
        <div
          className='avatar'
          style={{
            backgroundImage: currentUser?.avatar
              ? `url(${currentUser?.avatar})`
              : undefined,
          }}
        ></div>
      </div>
      <div className='profile-info'>
        <h4>@{currentUser?.username}</h4>
        <div className='stats'>
          <div className='stat'>
            <BootstrapIcon icon='eye-fill' />
            <span>{currentUser?.amount_of_lookups ?? 0}</span>
          </div>
          <div className='stat'>
            <BootstrapIcon icon='heart-fill' />
            <span>{currentUser?.amount_of_likes ?? 0}</span>
          </div>
          <Link to={'/profile/subscriptions'} className='stat'>
            <BootstrapIcon icon='people-fill' />
            <span>{currentUser?.subscriptions_amount ?? 0}</span>
          </Link>
          <Link to={'/profile/subscribers'} className='stat'>
            <BootstrapIcon icon='people' />
            <span>{currentUser?.following_amount ?? 0}</span>
          </Link>
        </div>
        <div className='buttons'>
          <Button
            isLink={true}
            primary={true}
            linkTo={'/profile/settings'}
            text={t('currentProfile.profileSettings')}
            icon='gear'
          />
          <Button
            isLink={true}
            linkTo={'/profile/orders'}
            text={t('currentProfile.myOrders')}
            icon='shop'
          />
        </div>
      </div>

      {currentUser?.favorite_prompts &&
        currentUser.favorite_prompts.length > 0 && (
          <div className='profile-products'>
            <h2>⭐ {t('currentProfile.favoritePrompts')}</h2>
            <br />
            <div className='cards-wrapper'>
              {(currentUser?.favorite_prompts ?? []).map((prompt, index) => {
                return (
                  <ProductCard
                    id={prompt.id}
                    creator={prompt.user ?? 0}
                    key={index}
                    category={prompt.model_category.name}
                    name={prompt.name}
                    price={prompt.price}
                    image={prompt.image}
                    is_liked={prompt.is_liked ?? false}
                    paid={prompt.purchased ?? false}
                  />
                );
              })}
            </div>
            <Button
              iconPosition='right'
              text={t('currentProfile.browseMarketplace')}
              icon='arrow-up-right'
            />
          </div>
        )}
    </div>
  );
}
