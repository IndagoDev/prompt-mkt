import './index.scss';

import { useEffect, useState } from 'react';

import { IPrompt } from '../../../../interfaces/IApi';
import { MarketplaceService } from '../../../../api/services/marketplaceService';
import { ProductCard } from '../../../../components/productCard';
import { useAsyncProcessManagerContext } from '../../../../tools/asyncProcessManager/context';
import { useTranslation } from 'react-i18next';

export default function MyOrdersPage() {
  const { t } = useTranslation();
  const asyncProcessManager = useAsyncProcessManagerContext();
  const [orders, setOrders] = useState<IPrompt[]>([]);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch current user orders',
      action: async () => {
        let response = await MarketplaceService.getMyOrders();

        if (response) {
          console.log(response);
          setOrders(response.results ?? []);
        }
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className='page orders'>
      <div className='profile-products'>
        <h2>{t('myOrdersPage.title')}</h2>
        <br />
        <div className='cards-wrapper'>
          {(orders ?? []).map((prompt, index) => {
            return (
              <ProductCard
                id={prompt.id}
                creator={prompt.user ?? 0}
                key={index}
                category={prompt.model_category.name}
                name={prompt.name}
                price={prompt.price}
                image={prompt.image}
                is_liked={prompt.is_liked ?? false}
                paid={prompt.purchased ?? false}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}
