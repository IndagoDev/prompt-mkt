import { IUser } from '../../../../../../interfaces/IApi';
import { Link } from 'react-router-dom';
import React from 'react';

interface Props {
  subscriber: IUser;
}

const SubscriberItem: React.FC<Props> = ({ subscriber }) => {
  const { username, avatar } = subscriber;

  return (
    <Link to={`/profile/${username}`} className="subscriber-item">
      <div
        className="subscriber-avatar"
        style={{ backgroundImage: `url(${avatar})` }}
      ></div>
      <p className="subscriber-username">{username}</p>
    </Link>
  );
};

export default SubscriberItem;
