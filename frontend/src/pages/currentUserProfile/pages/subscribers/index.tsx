import './index.scss';

import React, { useEffect, useState } from 'react';

import { Button } from '../../../../components/form/button';
import { IUser } from '../../../../interfaces/IApi';
import SubscriberItem from './components/item';
import { UserService } from '../../../../api/services/userService';
import { useAsyncProcessManagerContext } from '../../../../tools/asyncProcessManager/context';
import { useTranslation } from 'react-i18next';

export const SubscribersPage: React.FC = () => {
  const asyncProcessManager = useAsyncProcessManagerContext();
  const { t } = useTranslation();

  const [subscribers, setSubscribers] = useState<IUser[]>([]);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch subscribers',
      action: async () => {
        let newSubscribers = await UserService.getUserSubscribers();
        setSubscribers(newSubscribers);
      },
    });
  }, []);

  return (
    <div className='page subscribers'>
      <h1>{t('subscribersPage.title')}</h1>
      <Button
        isLink={true}
        linkTo={'/profile'}
        text={t('subscribersPage.goBackButton')}
        icon='chevron-left'
      />
      {subscribers.map((subscriber) => (
        <SubscriberItem key={subscriber.username} subscriber={subscriber} />
      ))}
    </div>
  );
};
