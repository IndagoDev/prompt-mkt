import './index.scss';

import {
  setCurrentUser,
  setIsAuthorized,
} from '../../../../store/slices/userSlice';
import { useEffect, useState } from 'react';

import { Button } from '../../../../components/form/button';
import { IAttachment } from '../../../../interfaces/IPromptCreation';
import { IUser } from '../../../../interfaces/IApi';
import { ImageUploader } from '../../../sell/steps/promptFile/components/imageUploader';
import { TextInput } from '../../../../components/form/textInput';
import { UserService } from '../../../../api/services/userService';
import { useAsyncProcessManagerContext } from '../../../../tools/asyncProcessManager/context';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function UserSettingsPage() {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const asyncProcessManager = useAsyncProcessManagerContext();
  const navigate = useNavigate();

  const [updatedUser, setUpdatedUser] = useState<IUser | null>(null);

  const [avatarImagesBuffer, setAvatarImagesBuffer] = useState<IAttachment[]>(
    []
  );
  const [backgroundImagesBuffer, setBackgroundImagesBuffer] = useState<
    IAttachment[]
  >([]);

  function handleOnSave() {
    asyncProcessManager?.doProcess({
      name: 'Save user settings',
      action: async () => {
        await UserService.saveUser({
          username: updatedUser?.username ?? '',
          avatar: avatarImagesBuffer[0]?.file ?? undefined,
          background_photo: backgroundImagesBuffer[0]?.file ?? undefined,
        });

        navigate('/profile');
      },
    });
  }

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch current user',
      action: async () => {
        let response = await UserService.getCurrentUser();

        if (response) {
          console.log(response);
          dispatch(setCurrentUser(response));
          dispatch(setIsAuthorized(true));
          setUpdatedUser(response);
        }
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className='page settings'>
      <div className='fade-in signup-step'>
        <div className='two-col'>
          <div className='left-col'>
            <h2>{t('userSettingsPage.title')}</h2>
            <div className='fields'>
              <div className='field'>
                <h4>{t('userSettingsPage.username')}</h4>
                <TextInput
                  type='text'
                  placeholder='@example'
                  value={updatedUser?.username ?? ''}
                  onChange={(e) => {
                    if (updatedUser) {
                      setUpdatedUser({
                        ...updatedUser,
                        username: e.target.value,
                      });
                    }
                  }}
                />
              </div>
              <div className='field'>
                <h4>{t('userSettingsPage.avatar')}</h4>
                <ImageUploader
                  maxImages={1}
                  images={avatarImagesBuffer}
                  onChange={(images) => {
                    setAvatarImagesBuffer(images);
                  }}
                />
              </div>
              <div className='field'>
                <h4>{t('userSettingsPage.backgroundImage')}</h4>
                <ImageUploader
                  maxImages={1}
                  images={backgroundImagesBuffer}
                  onChange={(images) => {
                    setBackgroundImagesBuffer(images);
                  }}
                />
              </div>
              <div className='field'>
                <Button
                  onClick={handleOnSave}
                  primary={true}
                  icon='save'
                  text={t('userSettingsPage.saveButton')}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
