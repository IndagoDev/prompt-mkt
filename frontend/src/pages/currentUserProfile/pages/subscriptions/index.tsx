import './index.scss';

import React, { useEffect, useState } from 'react';

import { Button } from '../../../../components/form/button';
import { IUser } from '../../../../interfaces/IApi';
import SubscriberItem from './components/item';
import { UserService } from '../../../../api/services/userService';
import { useAsyncProcessManagerContext } from '../../../../tools/asyncProcessManager/context';
import { useTranslation } from 'react-i18next';

export const SubscriptionsPage: React.FC = () => {
  const asyncProcessManager = useAsyncProcessManagerContext();
  const { t } = useTranslation();

  const [subscriptions, setSubscriptions] = useState<IUser[]>([]);

  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch subscriptions',
      action: async () => {
        let newSubscriptions = await UserService.getUserSubscriptions();
        setSubscriptions(newSubscriptions);
      },
    });
  }, []);

  return (
    <div className='page subscriptions'>
      <h1>{t('subscriptionsPage.title')}</h1>
      <Button
        isLink={true}
        linkTo={'/profile'}
        text={t('subscriptionsPage.goBackButton')}
        icon='chevron-left'
      />
      {subscriptions.map((subscriber) => (
        <SubscriberItem key={subscriber.username} subscriber={subscriber} />
      ))}
    </div>
  );
};
