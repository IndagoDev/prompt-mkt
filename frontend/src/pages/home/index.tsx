import './index.scss';

import { useEffect, useState } from 'react';

import { Button } from '../../components/form/button';
import { IGetMainPagePrompts } from '../../interfaces/IApi';
import { MarketplaceService } from '../../api/services/marketplaceService';
import { ProductCard } from '../../components/productCard';
import { useAsyncProcessManagerContext } from '../../tools/asyncProcessManager/context';
import { useTranslation } from 'react-i18next';

export function HomePage() {
  const asyncProcessManager = useAsyncProcessManagerContext();

  const [prompts, setPrompts] = useState<IGetMainPagePrompts | null>(null);
  useEffect(() => {
    asyncProcessManager?.doProcess({
      name: 'Fetch prompts',
      action: async () => {
        let newPrompts = await MarketplaceService.getMainPagePrompts();
        setPrompts(newPrompts);
      },
    });
  }, [asyncProcessManager]);

  const { t } = useTranslation();

  return (
    <div className='page home slides'>
      <div className='slide flex main'>
        <div className='slide-bg'></div>
        <div className='slide-content'>
          <h1>
            <span>{t('homePage.slides.mainSlide.title')}</span>
          </h1>
          <h3>
            <span>{t('homePage.slides.mainSlide.description')}</span>
          </h3>
          <div className='buttons-group'>
            <Button
              isLink={true}
              linkTo={'/marketplace/'}
              text={t('homePage.buttons.browseMarketplace')}
              icon='shop'
            />
          </div>
        </div>
      </div>
      <div className='slide flex limited-h'>
        <div className='slide-content'>
          <h2>{t('homePage.slides.featuredPromptsSlide.title')}</h2>
          <br />
          <div className='cards-wrapper'>
            {(prompts?.featured_prompts ?? []).map((prompt, index) => {
              return (
                <ProductCard
                  id={prompt.id}
                  creator={prompt.user ?? 0}
                  key={index}
                  category={prompt.model_category.name}
                  name={prompt.name}
                  price={prompt.price}
                  image={prompt.image}
                  is_liked={prompt.is_liked ?? false}
                  paid={prompt.purchased ?? false}
                />
              );
            })}
          </div>
          <Button
            iconPosition='right'
            text={t('homePage.buttons.browseMarketplace')}
            icon='arrow-up-right'
          />
        </div>
      </div>
      <div className='slide flex limited-h'>
        <div className='slide-content'>
          <h2>{t('homePage.slides.hottestPromptsSlide.title')}</h2>
          <br />
          <div className='cards-wrapper'>
            {(prompts?.top_prompts ?? []).map((prompt, index) => {
              return (
                <ProductCard
                  id={prompt.id}
                  creator={prompt.user ?? 0}
                  key={index}
                  category={prompt.model_category.name}
                  name={prompt.name}
                  price={prompt.price}
                  image={prompt.image}
                  is_liked={prompt.is_liked ?? false}
                  paid={prompt.purchased ?? false}
                />
              );
            })}
          </div>
          <Button
            iconPosition='right'
            text={t('homePage.buttons.browseMarketplace')}
            icon='arrow-up-right'
          />
        </div>
      </div>
      <div className='slide flex limited-h'>
        <div className='slide-content'>
          <h2>{t('homePage.slides.newPromptsSlide.title')}</h2>
          <br />
          <div className='cards-wrapper'>
            {(prompts?.top_prompts ?? []).map((prompt, index) => {
              return (
                <ProductCard
                  id={prompt.id}
                  creator={prompt.user ?? 0}
                  key={index}
                  category={prompt.model_category.name}
                  name={prompt.name}
                  price={prompt.price}
                  image={prompt.image}
                  is_liked={prompt.is_liked ?? false}
                  paid={prompt.purchased ?? false}
                />
              );
            })}
          </div>
          <Button
            iconPosition='right'
            text={t('homePage.buttons.browseMarketplace')}
            icon='arrow-up-right'
          />
        </div>
      </div>
    </div>
  );
}
