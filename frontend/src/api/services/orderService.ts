import {
  ILoginRequest,
  ILoginResponse,
  IRegisterRequest,
  IRegisterResponse,
} from '../../interfaces/IApi';

import { userApi } from '..';

export class AuthService {
  static async createOrder(data: ILoginRequest): Promise<ILoginResponse> {
    const response = await userApi.post<ILoginResponse>('/login/', data);
    return response.data;
  }

  static async register(data: IRegisterRequest): Promise<IRegisterResponse> {
    const response = await userApi.post<IRegisterResponse>('/register/', data);
    return response.data;
  }

  static async logout(): Promise<any> {
    const response = await userApi.get<any>('/logout/');
    return response.data;
  }
}
