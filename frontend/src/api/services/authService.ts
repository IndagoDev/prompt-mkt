import { GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login';
import {
  ILoginRequest,
  ILoginResponse,
  IRegisterRequest,
  IRegisterResponse,
} from '../../interfaces/IApi';

import { userApi } from '..';

export class AuthService {
  static async login(data: ILoginRequest): Promise<ILoginResponse> {
    const response = await userApi.post<ILoginResponse>('/login/', data);
    return response.data;
  }

  static async onGoogleLogin(
    response: GoogleLoginResponse | GoogleLoginResponseOffline
  ): Promise<ILoginResponse> {
    const apiResponse = await userApi.post<any>('/google-callback-login/', {
      ...response,
    });
    return apiResponse.data;
  }

  static async onGoogleSignUp(
    response: GoogleLoginResponse | GoogleLoginResponseOffline
  ): Promise<IRegisterResponse> {
    const apiResponse = await userApi.post<any>('/google-callback-signup/', {
      ...response,
    });
    return apiResponse.data;
  }

  static async register(data: IRegisterRequest): Promise<IRegisterResponse> {
    const response = await userApi.post<IRegisterResponse>('/register/', data);
    return response.data;
  }

  static async logout(): Promise<any> {
    const response = await userApi.get<any>('/logout/');
    return response.data;
  }
}
