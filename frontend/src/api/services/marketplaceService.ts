import {
  ICategory,
  ICreateAttachmentRequest,
  ICreateAttachmentResponse,
  ICreateLikeCreateRequest,
  ICreateLikeCreateResponse,
  ICreateOrderRequest,
  ICreateOrderResponse,
  ICreatePromptRequest,
  ICreatePromptResponse,
  ICreateTagRequest,
  ICreateTagResponse,
  IDeleteLikeRequest,
  IGetCategoriesResponse,
  IGetCustomParametersRequest,
  IGetCustomParametersResponse,
  IGetMainPagePrompts,
  IGetPromptRequest,
  IGetPromptResponse,
  IGetPromptsRequest,
  IGetPromptsResponse,
  IGetTagsResponse,
  IPromptsFavoriteResponse,
  ITag,
} from '../../interfaces/IApi';

import { RequestTools } from '../../tools/requestTools';
import { shopApi } from '..';

export class MarketplaceService {
  static async getTags(): Promise<ITag[]> {
    const response = await shopApi.get<IGetTagsResponse>('/get-tags/');
    return response.data.results;
  }

  static async createTag(data: ICreateTagRequest): Promise<ICreateTagResponse> {
    const response = await shopApi.post<ICreateTagResponse>(
      '/create-prompt-tag/',
      data
    );
    return response.data;
  }

  static async getCategories(): Promise<ICategory[]> {
    const response = await shopApi.get<IGetCategoriesResponse>(
      '/get-categories/'
    );
    return response.data.results;
  }

  static async getModelCategories(): Promise<ICategory[]> {
    const response = await shopApi.get<IGetCategoriesResponse>(
      '/get-model-categories/'
    );
    return response.data.results;
  }

  static async getMainPagePrompts(): Promise<IGetMainPagePrompts> {
    const response = await shopApi.get<IGetMainPagePrompts>('/main-page/');
    return response.data;
  }

  static async getFavouritePrompts(): Promise<IPromptsFavoriteResponse> {
    //in response prompts are under results IPromptFavoritesResponse
    const response = await shopApi.get<IPromptsFavoriteResponse>('/favorites/');
    return response.data;
  }

  static async getMyOrders(): Promise<IGetPromptsResponse> {
    //in response prompts are under results IPromptFavoritesResponse
    let response = await shopApi.get<any>('/orders/');
    if (response.data) {
      response.data.results = response.data?.results
        ? response.data.results.map((r: any) => {
            return r.prompt;
          })
        : response.data?.results;
    }
    return response.data;
  }

  static async getPrompts(
    data?: IGetPromptsRequest
  ): Promise<IGetPromptsResponse> {
    const response = await shopApi.get<IGetPromptsResponse>(
      `/search/?${RequestTools.buildQueryString(data)}`
    );
    return response.data;
  }

  static async getPrompt(
    data?: IGetPromptRequest
  ): Promise<IGetPromptResponse> {
    const response = await shopApi.get<IGetPromptResponse>(
      `/prompt/${data?.id}/`
    );
    return response.data;
  }

  static async createOrder(
    data?: ICreateOrderRequest
  ): Promise<ICreateOrderResponse> {
    const response = await shopApi.post<ICreateOrderResponse>(
      '/orders/create/',
      data
    );
    return response.data;
  }

  static async createPrompt(
    data: ICreatePromptRequest
  ): Promise<ICreatePromptResponse> {
    let formData = new FormData();

    for (var key in data) {
      let value = (data as any)[key];
      if (Array.isArray(value)) {
        // eslint-disable-next-line no-loop-func
        value.forEach((v) => {
          formData.append(key, v);
        });
      } else {
        formData.append(key, (data as any)[key]);
      }
    }
    const response = await shopApi.post<ICreatePromptResponse>(
      '/create-prompt/',
      formData
    );
    return response.data;
  }

  static async createAttachment(
    data: ICreateAttachmentRequest
  ): Promise<ICreateAttachmentResponse> {
    let formData = new FormData();

    for (var key in data) {
      formData.append(key, (data as any)[key]);
    }
    const response = await shopApi.post<ICreateAttachmentResponse>(
      '/create-attachment/',
      formData
    );
    return response.data;
  }

  static async likePrompt(
    data?: ICreateLikeCreateRequest
  ): Promise<ICreateLikeCreateResponse> {
    const response = await shopApi.post<ICreateLikeCreateResponse>(
      '/create-prompt-like/',
      data
    );
    return response.data;
  }

  static async deleteLikePrompt(data?: IDeleteLikeRequest): Promise<void> {
    const response = await shopApi.delete<void>(
      `/delete-prompt-like/${data?.promptId}/`
    );
    return response.data;
  }

  static async getCustomParameters(
    data: IGetCustomParametersRequest
  ): Promise<IGetCustomParametersResponse> {
    const response = await shopApi.get<IGetCustomParametersResponse>(
      `/get-model-category-create-params/${data?.id}`
    );
    return response.data;
  }
}
