import {
  ICurrentUser,
  IGetUserRequest,
  IMarkFavouriteRequest,
  IPartialUserUpdateRequest,
  IPartialUserUpdateResponse,
  ISubscriptionRequest,
  ISubscriptionResponse,
  IUnsubscribeRequest,
  IUser,
  IUserLikesRequest,
  IUserLookup,
  IUserSettingsResponse,
} from '../../interfaces/IApi';

import { RequestTools } from '../../tools/requestTools';
import { userApi } from '..';

export class UserService {
  static async getUser(data: IGetUserRequest): Promise<IUser> {
    const response = await userApi.get<IUser>(`/profile/${data.username}/`);
    return response.data;
  }

  static async getCurrentUser(): Promise<ICurrentUser | null> {
    try {
      const response = await userApi.get<ICurrentUser>('/profile/');
      return response.data;
    } catch (error) {
      const code = RequestTools.getApiErrorCode(error);

      if (code === 401) {
        return null;
      } else {
        throw error;
      }
    }
  }

  static async updateUser(data: IPartialUserUpdateRequest): Promise<void> {
    await userApi.patch<IPartialUserUpdateResponse>(`/partial-update/`, data);
  }

  static async markPromptFavorite(data: IMarkFavouriteRequest): Promise<void> {
    await userApi.put<IMarkFavouriteRequest>(`/favorite/`, data);
  }

  static async getUserSettings(): Promise<void> {
    await userApi.get<IUserSettingsResponse>(`/settings/`);
  }

  static async getUserSubscribers(): Promise<IUser[]> {
    //use IUserProfile but under results if I remember correctly
    const response = await userApi.get(`/get-my-subscribers/`);
    return response.data.results ?? [];
  }

  static async getUserSubscriptions(): Promise<IUser[]> {
    //use IUserProfile but under results if I remember correctly
    const response = await userApi.get(`/get-my-subscriptions/`);
    return response.data.results ?? [];
  }

  static async subscribe(data: ISubscriptionRequest): Promise<void> {
    //use IUserProfile but under results if I remember correctly
    await userApi.post<ISubscriptionResponse>(`/subscribe/`, data);
  }

  static async unsubscribe(data: IUnsubscribeRequest): Promise<void> {
    await userApi.delete(`/unsubscribe/${data.username}/`);
  }

  static async updateUserLookups(data: IUserLookup): Promise<void> {
    await userApi.get(
      `/update-user-lookups/?${RequestTools.buildQueryString(data)}`
    );
  }

  static async createUserLike(data: IUserLikesRequest): Promise<void> {
    await userApi.post(`/create-user-like/`, data);
  }
  static async deleteUserLike(data: { username: string }): Promise<void> {
    await userApi.post(`/delete-user-like/${data.username}/`, data);
  }
  static async saveUser(
    data: IPartialUserUpdateRequest
  ): Promise<IPartialUserUpdateResponse> {
    let formData = new FormData();

    for (var key in data) {
      let value = (data as any)[key];

      if (value !== undefined) formData.append(key, value);
    }
    let response = await userApi.put<IPartialUserUpdateResponse>(
      `/partial-update/`,
      formData
    );
    return response.data;
  }
}
