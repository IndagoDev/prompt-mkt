import axios from 'axios';

let authToken = localStorage.getItem('auth');

export const rootApi = axios.create({
  baseURL: process.env.REACT_APP_API_URI,
});

export const userApi = axios.create({
  baseURL: process.env.REACT_APP_API_URI + '/user',
  headers: {
    Authorization: authToken ? `token ${authToken}` : undefined,
  },
});

export const shopApi = axios.create({
  baseURL: process.env.REACT_APP_API_URI + '/shop',
  headers: {
    Authorization: authToken ? `token ${authToken}` : undefined,
  },
});

userApi.interceptors.request.use(function (config) {
  const authToken = localStorage.getItem('auth');
  config.headers.Authorization = authToken ? `token ${authToken}` : undefined;

  return config;
});

shopApi.interceptors.request.use(function (config) {
  const authToken = localStorage.getItem('auth');
  config.headers.Authorization = authToken ? `token ${authToken}` : undefined;

  return config;
});
