import { Navigate, Route, Routes } from 'react-router';

import { Contact } from './pages/contact';
import { CookiePolicy } from './pages/cookie';
import CurrentUserProfile from './pages/currentUserProfile';
import { FAQ } from './pages/faq';
import { HirePage } from './pages/hire';
import { HomePage } from './pages/home';
import { LogOutPage } from './pages/auth/logout';
import { LoginPage } from './pages/auth/login';
import { MarketplacePage } from './pages/marketplace';
import MyOrdersPage from './pages/currentUserProfile/pages/orders';
import { NotFoundPage } from './pages/404';
import { PrivacyPolicy } from './pages/privacy';
import Profile from './pages/profile';
import { PromptDetails } from './pages/marketplace/prompt';
import { RootState } from './store/store';
import { SellPage } from './pages/sell';
import { SignUpPage } from './pages/auth/signup';
import { SubscribersPage } from './pages/currentUserProfile/pages/subscribers';
import { SubscriptionsPage } from './pages/currentUserProfile/pages/subscriptions';
import { TermsOfUse } from './pages/terms';
import UserSettingsPage from './pages/currentUserProfile/pages/settings';
import { useSelector } from 'react-redux';

export function AppRouter() {
  const isAuthorized = useSelector(
    (state: RootState) => state.user.isAuthorized
  );

  return (
    <Routes>
      <Route path='/'>
        <Route index element={<Navigate to={'home'} />} />
        <Route path='home' element={<HomePage />} />
        <Route path='hire' element={<HirePage />} />
        <Route path='marketplace' element={<MarketplacePage />} />
        <Route path='marketplace/:id' element={<PromptDetails />} />
        {isAuthorized ? (
          <>
            <Route path='sell' element={<SellPage />} />
            <Route path='profile' element={<CurrentUserProfile />} />
            <Route path='profile/orders' element={<MyOrdersPage />} />
            <Route path='profile/settings' element={<UserSettingsPage />} />
            <Route path='profile/subscribers' element={<SubscribersPage />} />
            <Route
              path='profile/subscriptions'
              element={<SubscriptionsPage />}
            />
            <Route path='profile/:username' element={<Profile />} />
          </>
        ) : (
          <>
            <Route path='sell' element={<Navigate to='/auth' />} />
            <Route path='profile' element={<Navigate to='/auth' />} />
            <Route path='profile/*' element={<Navigate to='/auth' />} />
          </>
        )}
        <Route path='faq' element={<FAQ />} />
        <Route path='contact' element={<Contact />} />
        <Route path='privacy' element={<PrivacyPolicy />} />
        <Route path='cookie' element={<CookiePolicy />} />
        <Route path='terms' element={<TermsOfUse />} />

        <Route path='auth'>
          {!isAuthorized ? (
            <>
              <Route path='login' element={<LoginPage />} />
              <Route path='signup' element={<SignUpPage />} />
              <Route path='*' index element={<Navigate to={'login'} />} />
            </>
          ) : (
            <>
              <Route path='logout' element={<LogOutPage />} />
              <Route path='*' index element={<Navigate to={'/profile'} />} />
            </>
          )}
        </Route>
      </Route>
      <Route path='*' element={<NotFoundPage />} />
    </Routes>
  );
}
