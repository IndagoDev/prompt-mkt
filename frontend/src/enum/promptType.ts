export enum PromptType {
  GPT = "GPT / ChatGPT",
  DALLE = "DALL·E",
  Midjourney = "Midjourney",
}
