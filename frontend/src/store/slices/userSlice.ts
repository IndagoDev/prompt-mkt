// src/store/slices/userSlice.ts

import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { ICurrentUser } from '../../interfaces/IApi';

interface UserState {
  isAuthorized: boolean;
  currentUser: ICurrentUser | null;
  loading: boolean;
  error: string | null;
}

const initialState: UserState = {
  isAuthorized: false,
  currentUser: null,
  loading: false,
  error: null,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setIsAuthorized: (state, action: PayloadAction<boolean>) => {
      state.isAuthorized = action.payload;
    },
    setCurrentUser: (state, action: PayloadAction<ICurrentUser | null>) => {
      state.currentUser = action.payload;
    },
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    setError: (state, action: PayloadAction<string | null>) => {
      state.error = action.payload;
    },
  },
});

export const { setIsAuthorized, setCurrentUser, setLoading, setError } = userSlice.actions;

export default userSlice.reducer;
