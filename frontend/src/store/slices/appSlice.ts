// src/store/slices/appSlice.ts

import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { IPromptBuyDialog } from '../../components/app/promptBuyDialog';

interface AppState {
  searchQuery: string;
  promptBuyDialog: IPromptBuyDialog;
}

const initialState: AppState = {
  searchQuery: '',
  promptBuyDialog: {
    active: false,
    id: -1,
    amount: 0,
    currency: '$',
    creator: -1
  },
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setSearchQuery: (state, action: PayloadAction<string>) => {
      state.searchQuery = action.payload;
    },
    setPromptBuyDialog: (state, action: PayloadAction<IPromptBuyDialog>) => {
      state.promptBuyDialog = action.payload;
    },
  },
});

export const { setSearchQuery, setPromptBuyDialog } = appSlice.actions;

export default appSlice.reducer;
