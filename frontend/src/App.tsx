import './App.scss';

import { setCurrentUser, setIsAuthorized } from './store/slices/userSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

import { AppRouter } from './AppRouter';
import { AsyncProcessManagerContext } from './tools/asyncProcessManager/context';
import { BrowserRouter } from 'react-router-dom';
import { Footer } from './components/app/footer';
import { Header } from './components/app/header';
import { LoadingOverlay } from './components/loading';
import { PromptBuyDialog } from './components/app/promptBuyDialog';
import { RootState } from './store/store';
import { UserService } from './api/services/userService';
import toast from 'react-hot-toast';
import { useAsyncProcessManager } from './tools/asyncProcessManager';

function App() {
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const promptBuyDialog = useSelector(
    (state: RootState) => state.app.promptBuyDialog
  );

  const asyncProcessManager = useAsyncProcessManager({
    onFirstActiveStart: () => {
      setIsLoading(true);
    },
    onAllFinished: () => {
      setIsLoading(false);
    },
    onError: (_e, _, message, process) => {
      toast.error(`🤖 API error '${message}' on ${process.name}`);
    },
  });

  
  useEffect(() => {
    asyncProcessManager.doProcess({
      name: 'Fetch current user',
      action: async () => {
        let response = await UserService.getCurrentUser();

        if (response) {
          console.log(response);
          dispatch(setCurrentUser(response));
          dispatch(setIsAuthorized(true));
        }
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [asyncProcessManager]);

  return (
    <AsyncProcessManagerContext.Provider value={asyncProcessManager}>
      <BrowserRouter>
        <div className='app'>
          <Header />
          <AppRouter />
          <Footer />
          <PromptBuyDialog {...promptBuyDialog} />
          <LoadingOverlay active={isLoading} />
        </div>
      </BrowserRouter>
    </AsyncProcessManagerContext.Provider>
  );
}

export default App;
