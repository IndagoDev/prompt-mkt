import { GPTType } from '../enum/gptType';

export interface IInitialData {
  promptType: number | null;
  name: string;
  description: string;
  estimatedPrice: string | null;
  exampleInput: string;
  exampleOutput: string;
  selectedCategories: number[];
  selectedTags: number[];
}

export interface IMainData {
  prompt: string;
  promptInstructions: string;
  customFields: any;
}

export interface IAttachment {
  id: string;
  src: string;
  file: File;
  apiId: number | undefined;
}

export interface IMainDataGPT extends IMainData {
  gptType: GPTType;
}

export interface IMainDataVisual extends IMainData {
  attachments: IAttachment[];
}

export interface IMainDataDALLE extends IMainDataVisual {
  verificationLink: string;
}

export interface IMainDataMidjourney extends IMainDataVisual {
  profileLink: string;
}

export interface IPromptCreation {
  initialData: IInitialData;
  mainData:
    | IMainData
    | IMainDataGPT
    | IMainDataVisual
    | IMainDataDALLE
    | IMainDataMidjourney;
}

export type IMainDataGeneric =
  | IMainData
  | IMainDataGPT
  | IMainDataVisual
  | IMainDataDALLE
  | IMainDataMidjourney;
