// Login
export interface ILoginRequest {
  email: string;
  password: string;
}

export interface ILoginResponse {
  auth_token: string;
  user: ICurrentUser;
}

// Register
export interface IRegisterRequest {
  username: string;
  email: string;
  password: string;
}

export interface IRegisterResponse {
  auth_token: string;
  user: ICurrentUser;
}

// Partial user update
export interface IPartialUserUpdateRequest {
  username?: string;
  avatar?: File;
  background_photo?: File;
  social_links?: Record<string, string>;
  custom_prompt_price?: number;
  register_provider?: string;
  sale_notification_emails?: boolean;
  new_favorites_emails?: boolean;
  new_followers_emails?: boolean;
  new_messages_emails?: boolean;
  new_job_emails?: boolean;
  new_review_emails?: boolean;
  new_credits_emails?: boolean;
  review_reminder_emails?: boolean;
  following_users_new_prompts?: boolean;
}

// Partial user update
export interface IPartialUserUpdateResponse {
  id?: number;
  username?: string;
  avatar?: File | null;
  background_photo?: File | null;
  social_links?: Record<string, string> | null;
  custom_prompt_price?: number;
  register_provider?: string;
  sale_notification_emails?: boolean;
  new_favorites_emails?: boolean;
  new_followers_emails?: boolean;
  new_messages_emails?: boolean;
  new_job_emails?: boolean;
  new_review_emails?: boolean;
  new_credits_emails?: boolean;
  review_reminder_emails?: boolean;
  following_users_new_prompts?: boolean;
}

// Create User and Prompt Like
export interface ICreateLikeCreateRequest {
  sender: number;
  receiver: number;
}

export interface ICreateLikeCreateResponse {
  sender: number;
  receiver: number;
  creation_date: number;
}

export interface IDeleteLikeRequest {
  promptId: number;
}

// Subscription
export interface ISubscriptionRequest {
  sender: number;
  receiver: number;
}

export interface ISubscriptionResponse {
  sender: number;
  receiver: number;
  creation_date: number;
}

//unsubscribe from user unsubscribe/
export interface IUnsubscribeRequest {
  username: string;
}
// Create order
export interface ICreateOrderRequest {
  prompt: number;
  buyer: number;
  creator: number;
  price: string;
}

export interface ICreateOrderResponse {
  prompt: number;
  buyer: number;
  creator: number;
  price: string;
  created_at: number;
}

// Generate Payment Widget
export interface IGeneratePaymentWidgetRequest {
  product_names: string;
  amount: number;
  currency: string;
  order_pk: number;
  product_cost: string;
  product_count: number;
  language: string;
}

// Finish Order
export interface IFinishOrderRequest {
  order_pk: number;
}

// Mark favourite
export interface IMarkFavouriteRequest {
  prompt_id: number;
  favorite?: boolean;
}

// Create prompt
export interface ICreatePromptRequest {
  model_category: number;
  name: string;
  image: File;
  attachments: number[];
  token_size: string;
  description: string;
  example_input: string;
  user: number;
  example_output: string;
  prompt_template: string;
  instructions: string;
  categories: number[];
  sell_amount: number;
  tags: number[];
  custom_parameters?: any;
}

export interface ICreatePromptResponse {
  id: number;
  image: string;
  model_category: number;
  price: string;
  name: string;
  description: string;
  token_size: number;
  example_input: string;
  example_output: string;
  user: number;
  categories: number[];
  sell_amount: string;
  tags: number[];
  attachments: number[];
  prompt_template: string;
  instructions: string;
  custom_parameters: null | Record<string, string>;
}

// Create Attachment
export interface ICreateAttachmentRequest {
  file_type: number;
  _file: File;
}

export interface ICreateAttachmentResponse {
  id: number;
  file_type: number;
  _file: File;
}

// Create tag
export interface ICreateTagRequest {
  is_system: boolean;
  name: string;
  icon?: File;
}

export interface ICreateTagResponse {
  id: number;
  is_system: boolean;
  name: string;
  icon: File;
}

// User
export interface IUser {
  id: number;
  username: string;
  avatar: string | null;
  background_photo: string | null;
  amount_of_lookups: number;
  amount_of_likes: number;
  amount_of_sells: number;
  most_popular_prompts?: IPrompt[];
  newest_prompts?: IPrompt[];
  is_in_subscriptions?: boolean;
}

export interface ICurrentUser extends IUser {
  email: string;
  first_name: string;
  last_name: string;
  social_links: any;
  joined_date: number;
  custom_prompt_price: string;
  register_provider: string;
  sale_notification_emails: boolean;
  new_favorites_emails: boolean;
  new_followers_emails: boolean;
  new_messages_emails: boolean;
  new_job_emails: boolean;
  new_review_emails: boolean;
  new_credits_emails: boolean;
  review_reminder_emails: boolean;
  following_users_new_prompts: boolean;
  favorite_prompts: IPrompt[];
  subscriptions_amount: number;
  following_amount: number;
  orders: IPrompt[];
}

// UserLike
export interface IUserLike {
  id: number;
  sender: IUser;
  receiver: IUser;
  created_at: string;
}

// GetUser
export interface IGetUserRequest {
  username: string;
}

// ListUsers
export interface IListUsersRequest {
  page: number;
  per_page: number;
}

export interface IListUsersResponse {
  users: IUser[];
  total_count: number;
}

// UserLikes
export interface IUserLikesRequest {
  user_id: number;
}

export interface IUserLikesResponse {
  likes: IUserLike[];
}

// ListSubscriptions
export interface IListSubscriptionsRequest {
  user_id: number;
}

export interface IListSubscriptionsResponse {
  subscriptions: ISubscriptionResponse[];
}

// Tags
export interface ITag {
  id: number;
  name: string;
}

// Categories
export interface ICategory {
  id: number;
  name: string;
}

export interface IGetTagsResponse {
  results: ITag[];
}

export interface IGetCategoriesResponse {
  results: ICategory[];
}

export interface IModelCategory {
  id: number;
  name: string;
  icon: any;
}

export interface ICreateOrderRequest {
  prompt: number;
  buyer: number;
  creator: number;
  price: string;
}

export interface IGetPromptsRequest {
  tags?: number[];
  categories?: number[];
  model_categories?: number[];
  limit?: number;
  name?: string;
  sort_by?: string;
}

export interface IGetPromptsResponse {
  count: number;
  next: any;
  previous: any;
  results: IPrompt[];
}

export interface IGetMainPagePrompts {
  top_prompts: IPrompt[];
  featured_prompts: IPrompt[];
  new_prompts: IPrompt[];
}

export interface IBackendAttachment {
  id: number;
  file_type: string;
  _file: string;
}

export interface IGetPromptRequest {
  id: number;
}

export interface IGetPromptResponse extends IPrompt {}

export interface IPrompt {
  id: number;
  image: string;
  model_category: IModelCategory;
  price: string;
  name: string;
  description?: string;
  token_size?: number;
  example_input?: string;
  example_output?: string;
  user?: number;
  review_amount?: number;
  creation_date?: string;
  tags?: ITag[];
  amount_of_lookups?: number;
  ratings?: any[];
  attachments?: IBackendAttachment[];
  prompt_template?: string;
  instructions?: string;
  purchased?: boolean;
  categories?: ICategory[];
  is_liked?: boolean;
}

//Rating response
export interface IRating {
  id: number;
  amount_of_stars: number;
  prompt: number;
}

//favorite response for prompt favorites/
export interface IPromptFavoritesResponse {
  id: number;
  image: string;
  model_category: IModelCategory;
  price: string;
  name: string;
  description: string;
  token_size: number;
  example_input: string;
  example_output: string;
  user: number;
  review_amount: number;
  creation_date: string;
  tags: ITag[];
  amount_of_lookups: number;
  ratings: IRating[];
  attachments: ICreateAttachmentResponse[];
  prompt_template: string;
  instructions: string;
  purchased: boolean;
  categories: ICategory[];
}

export interface IPromptsFavoriteResponse {
  results: IPromptFavoritesResponse[];
}

export interface IPromptLarge {
  id: number;
  image: string;
  model_category: IModelCategory;
  price: string;
  name: string;
  description: string;
  token_size: number;
  example_input: string;
  example_output: string;
  user: number;
  review_amount: number;
  creation_date: string;
  tags: ITag[];
  amount_of_lookups: number;
  ratings: any[];
  attachments: any[];
  prompt_template: string;
  instructions: string;
  purchased: boolean;
}

//order interface for order history orders/

export interface IPromptOrder {
  id: number;
  image: string;
  model_category: IModelCategory;
  price: string;
  name: string;
  description: string;
  token_size: number;
  example_input: string;
  example_output: string;
  user: number;
  review_amount: number;
  creation_date: string;
  tags: ITag[];
  amount_of_lookups: number;
  ratings: IRating[];
  attachments: ICreateAttachmentResponse[];
  prompt_template: string;
  instructions: string;
  purchased: boolean;
  categories: ICategory[];
}

//you can use this interface also for : get subscribers and subscriptions
export interface IUserProfile {
  id: number;
  username: string;
  avatar: string | null;
  background_photo: string | null;
  amount_of_lookups: number;
  amount_of_likes: number;
  amount_of_sells: number;
}
export interface IOrdersResponse {
  id: number;
  buyer: IUserProfile;
  prompt: IPromptOrder;
  creator: IUserProfile;
  price: number;
  created_at: number;
}

//settings user settings/

export interface IUserSettingsResponse {
  id: number;
  social_links: Record<string, string>;
  custom_prompt_price: number;
  sale_notification_emails: boolean;
  new_favorites_emails: boolean;
  new_followers_emails: boolean;
  new_messages_emails: boolean;
  new_job_emails: boolean;
  new_review_emails: boolean;
  new_credits_emails: boolean;
  review_reminder_emails: boolean;
  following_users_new_prompts: boolean;
}

// own profile interface profile/

interface ICustomUserResponse {
  id: number;
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
  background_photo: string;
  social_links: Record<string, string>;
  amount_of_lookups: number;
  amount_of_likes: number;
  joined_date: string;
  custom_prompt_price: string;
  register_provider: string;
  sale_notification_emails: boolean;
  new_favorites_emails: boolean;
  new_followers_emails: boolean;
  new_messages_emails: boolean;
  new_job_emails: boolean;
  new_review_emails: boolean;
  new_credits_emails: boolean;
  review_reminder_emails: boolean;
  following_users_new_prompts: boolean;
  favorite_prompts: IPrompt[];
}

//User lookup interface for user update-user-lookups/ query param

export interface IUserLookup {
  username: string;
}

//User lookup interface for prompt update-prompt-lookups/ query param

export interface IPromptLookup {
  pk: number;
}

// delete prompt like interface for prompt delete-prompt-like/<int:pk> -> only pk in url

export interface IGetCustomParametersRequest {
  id: number;
}

export interface IGetCustomParametersResponse {
  id: number;
  name: string;
  icon: any;
  custom_parameters: ICustomParameters;
}

export interface ICustomParameters {
  fields: ICustomParametersField[];
}

export interface ICustomParametersField {
  name: string;
  type: string;
  label_en: string;
  label_ua: string;
  validation: ICustomParametersFieldValidation;
  placeholder: string;
}

export interface ICustomParametersFieldValidation {
  type: string;
}
