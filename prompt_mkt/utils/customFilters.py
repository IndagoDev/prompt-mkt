from django_filters import rest_framework as filters
from apps.shop.models import Prompt, Category
from apps.users.models import User


class PromptFilter(filters.FilterSet):
    model = filters.CharFilter(field_name='model_category__name', lookup_expr='icontains')
    categories = filters.ModelMultipleChoiceFilter(lookup_expr='in', queryset=Category.objects.all(), field_name='categories')

    class Meta:
        model = Prompt
        fields = ['model', 'categories']

    def filter_by_category(self, queryset, name, value):
        print(value)
        print(name)
        return queryset.filter(categories__in=value)


class UserFilter(filters.FilterSet):
    username = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = User
        fields = (
            'username',
        )


class PromptNameFilter(filters.FilterSet):
    name = filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Prompt
        fields = (
            'name',
        )
