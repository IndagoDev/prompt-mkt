from rest_framework import serializers
from apps.shop.models import ModelCategory, Tag, Attachment, Rating, Prompt, Category, Order, PromptLike
from apps.users.models import User
from prompt_mkt.utils.customFields import TimestampField
from apps.users.serializers import UserGetProfileSerializer, CustomPromptSerializer


class ModelCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelCategory
        fields = ('id', 'name', 'icon')


class ModelCategoryCreatePromptSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelCategory
        fields = ('id', 'name', 'icon', 'custom_parameters')


class ModelCategoryGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelCategory
        fields = ('id', 'name')


class CategoryGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class TagGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'is_system', 'icon')


class AttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attachment
        fields = 'id', 'file_type', '_file'


class AttachmentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attachment
        fields = 'id', 'file_type', '_file'


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = ('id', 'amount_of_stars', 'prompt')


class PromptSerializer(serializers.ModelSerializer):
    model_category = ModelCategorySerializer()
    tags = TagSerializer(many=True)
    ratings = RatingSerializer(many=True)
    attachments = AttachmentSerializer(many=True)
    purchased = serializers.SerializerMethodField()
    categories = CategoryGetSerializer(many=True)
    is_liked = serializers.SerializerMethodField()

    class Meta:
        model = Prompt
        fields = (
            'id', 'image', 'model_category', 'name', 'price',
            'description', 'example_input',
            'example_output', 'user', 'review_amount',
            'creation_date', 'tags', 'amount_of_lookups', 'ratings',
            'attachments', 'prompt_template', 'instructions', 'purchased', 'categories', 'is_liked'
        )

    def get_purchased(self, obj):
        user = self.context.get('request').user
        if user.is_authenticated:
            return Order.objects.filter(
                buyer=user, prompt=obj
            ).exists() or Order.objects.filter(creator=user, prompt=obj).exists()
        return False

    def get_is_liked(self, obj: Prompt):
        user = self.context.get('request').user
        if user.is_authenticated:
            return PromptLike.objects.filter(
                sender=user, receiver=obj
            ).exists()
        return False


class PromptLessSerializer(serializers.ModelSerializer):
    model_category = ModelCategorySerializer()
    tags = TagSerializer(many=True)
    ratings = RatingSerializer(many=True)
    attachments = AttachmentSerializer(many=True)
    purchased = serializers.SerializerMethodField()
    categories = CategoryGetSerializer(many=True)
    is_liked = serializers.SerializerMethodField()

    class Meta:
        model = Prompt
        fields = (
            'id', 'image', 'model_category', 'price', 'name',
            'description',
            'example_output', 'user', 'review_amount',
            'creation_date', 'tags', 'amount_of_lookups', 'ratings',
            'attachments', 'purchased', 'categories', 'is_liked'
        )

    def get_purchased(self, obj):
        user = self.context.get('request').user
        if user.is_authenticated:
            return Order.objects.filter(
                buyer=user, prompt=obj
            ).exists() or Order.objects.filter(creator=user, prompt=obj).exists()
        return False

    def get_is_liked(self, obj: Prompt):
        user = self.context.get('request').user
        if user.is_authenticated:
            return PromptLike.objects.filter(
                sender=user, receiver=obj
            ).exists()
        return False


class MainPageSerializer(serializers.Serializer):
    featured_prompts = PromptSerializer(many=True)
    new_prompts = PromptSerializer(many=True)
    top_prompts = PromptSerializer(many=True)



class PromptCreateSerializer(serializers.ModelSerializer):
    model_category = serializers.PrimaryKeyRelatedField(queryset=ModelCategory.objects.all())
    categories = serializers.PrimaryKeyRelatedField(many=True, queryset=Category.objects.all())
    tags = serializers.PrimaryKeyRelatedField(many=True, queryset=Tag.objects.all())
    attachments = serializers.PrimaryKeyRelatedField(many=True, queryset=Attachment.objects.all(), required=False)
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    image = serializers.ImageField()
    custom_parameters = serializers.JSONField(required=False)

    class Meta:
        model = Prompt
        fields = (
            'id', 'image', 'model_category', 'price', 'name',
            'description', 'example_input',
            'example_output', 'user', 'categories', 'sell_amount',
            'tags', 'attachments', 'prompt_template', 'instructions', 'custom_parameters'
        )


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class ModelCategoryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelCategory
        fields = ('id', 'name')


class MarketplacePromptSerializer(serializers.ModelSerializer):
    model_category = ModelCategoryListSerializer()
    category = CategorySerializer(many=True)

    class Meta:
        model = Prompt
        fields = ('id', 'name', 'model_category', 'category', 'image')


class OrderSerializer(serializers.ModelSerializer):
    prompt = serializers.PrimaryKeyRelatedField(queryset=Prompt.objects.all())
    buyer = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    creator = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    created_at = TimestampField(required=False)

    class Meta:
        model = Order
        fields = ('id', 'buyer', 'prompt', 'creator', 'price', 'created_at')


class UserOrderSerializer(serializers.ModelSerializer):
    prompt = PromptSerializer()
    creator = UserGetProfileSerializer()
    created_at = TimestampField(required=False)
    buyer = UserGetProfileSerializer()

    class Meta:
        model = Order
        fields = ('id', 'buyer', 'prompt', 'creator', 'price', 'created_at')


class PromptLikeCreateSerializer(serializers.ModelSerializer):
    sender = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    receiver = serializers.PrimaryKeyRelatedField(queryset=Prompt.objects.all())
    creation_date = TimestampField(required=False)

    class Meta:
        model = PromptLike
        fields = '__all__'



class CustomUserSerializer(serializers.ModelSerializer):
    favorite_prompts = serializers.SerializerMethodField()
    joined_date = TimestampField()
    avatar = serializers.ImageField(use_url=True)
    background_photo = serializers.ImageField(use_url=True)
    subscriptions_amount = serializers.SerializerMethodField()
    following_amount = serializers.SerializerMethodField()
    orders = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'first_name', 'last_name', 'avatar', 'background_photo', 'social_links',
            'amount_of_lookups', 'amount_of_likes', 'joined_date', 'custom_prompt_price', 'register_provider',
            'sale_notification_emails', 'new_favorites_emails', 'favorite_prompts', 'subscriptions_amount',
            'following_amount', 'orders'
        )

    def get_subscriptions_amount(self, obj):
        return obj.subscribers.count()

    def get_following_amount(self, obj):
        return obj.subscriptions.count()

    def get_favorite_prompts(self, obj):
        return CustomPromptSerializer(
            obj.favorited_by.all(), many=True, context={'request': self.context.get('request')}
        ).data

    def get_orders(self, obj: User):
        prompts_in_orders = Prompt.objects.filter(pk__in=obj.orders.all().values_list('prompt__pk', flat=True))
        return CustomPromptSerializer(
            prompts_in_orders, many=True, context={'request': self.context.get('request')}
        ).data[:8]
