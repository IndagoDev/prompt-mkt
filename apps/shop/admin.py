from django.contrib import admin
from .models import ModelCategory, Tag, Attachment, Rating, Prompt, Category, PromptLike
from django.db import models
from django_json_widget.widgets import JSONEditorWidget

@admin.register(ModelCategory)
class ModelCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'icon')
    search_fields = ('name',)
    formfield_overrides = {
        # fields.JSONField: {'widget': JSONEditorWidget}, # if django < 3.1
        models.JSONField: {'widget': JSONEditorWidget},
    }


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_system', 'icon')
    list_filter = ('is_system',)
    search_fields = ('name',)


@admin.register(Attachment)
class AttachmentAdmin(admin.ModelAdmin):
    list_display = ('file_type', '_file')
    search_fields = ('file_type',)


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ('amount_of_stars', 'prompt')
    list_filter = ('amount_of_stars',)
    search_fields = ('prompt__name',)


@admin.register(Prompt)
class PromptAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'model_category', 'sell_amount', 'creation_date', 'review_amount', 'amount_of_lookups')
    list_filter = ('user', 'model_category', 'tags', 'creation_date')
    search_fields = ('name',)
    autocomplete_fields = ('user', 'model_category', 'tags', 'attachments',)
    formfield_overrides = {
        # fields.JSONField: {'widget': JSONEditorWidget}, # if django < 3.1
        models.JSONField: {'widget': JSONEditorWidget},
    }


@admin.register(PromptLike)
class TwoUserActionsAdmin(admin.ModelAdmin):
    model = PromptLike
    list_display = ('sender', 'receiver', 'created_date')
    search_fields = ('sender__username', 'receiver__name', 'sender__email')
