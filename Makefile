.PHONY: docs clean

COMMAND = docker-compose exec web python manage.py
PRODUCTION_COMMAND = docker-compose -f docker-compose.prod.yml exec web python manage.py

all: build run collectstatic database-setup


collectstatic:
	$(COMMAND) collectstatic --no-input

build:
	docker-compose build

create-super-user:
	$(COMMAND) createsuperuser

run:
	docker-compose up

run-d:
	docker-compose up -d

run-build:
	docker-compose up --build

run-d-build:
	docker-compose up -d --build
	$(COMMAND) collectstatic --no-input

production-run-d-build:
	docker-compose -f docker-compose-prod.yml up -d

down:
	docker-compose down

database-initial-migrations:
	$(COMMAND) makemigrations users
	$(COMMAND) makemigrations shop
	$(COMMAND) makemigrations blog
	$(COMMAND) migrate

database-migrations:
	$(COMMAND) makemigrations
	$(COMMAND) migrate

dockerclean:
	docker system prune -f
	docker system prune -f --volumes


logs_web:
	docker-compose logs web

logs_db:
	docker-compose logs db

logs_nginx:
	docker-compose logs nginx

production-build:
	 docker-compose -f docker-compose.prod.yml up -d --build

production-migrate:
	docker-compose -f docker-compose.prod.yml down -v
	docker-compose -f docker-compose.prod.yml up -d --build
	$(PRODUCTION_COMMAND) makemigrations
	$(PRODUCTION_COMMAND) migrate --noinput

mac-m1-build:
	export DOCKER_DEFAULT_PLATFORM=linux/amd64
	docker pull --platform linux/amd64 nginx:latest
	docker pull --platform linux/arm64 postgres:15-alpine
	docker pull --platform linux/amd64 redis:latest
	docker-compose down
	docker-compose build
	docker-compose up


remove-related-data:
	docker-compose down --volumes
	docker-compose rm -s -f
	docker-compose down --rmi all

remove-related-data-no-volumes:
	docker-compose rm -s -f
	docker-compose down --rmi all
