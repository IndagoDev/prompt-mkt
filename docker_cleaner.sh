#!/bin/sh

if [ "$#" -ne 2 ]; then
  echo "Usage: ./docker_cleanup.sh compose-file your_container_name your_image_name"
  exit 1
fi

COMPOSE_FILE="$1"
your_container_name="$2"
your_image_name="$3"

container_ids=$(docker ps -a | grep "$your_container_name" | awk '{print $1}')
if [ -n "$container_ids" ]; then
  echo "Stopping and removing containers:"
  echo "$container_ids"
  echo "$container_ids" | xargs docker stop | xargs docker rm
else
  echo "No containers found with the specified name."
fi

docker-compose -f $COMPOSE_FILE down

image_ids=$(docker images | grep "$your_image_name" | awk '{print $3}')
if [ -n "$image_ids" ]; then
  echo "Removing images:"
  echo "$image_ids"
  echo "$image_ids" | xargs docker rmi
else
  echo "No images found with the specified name."
fi